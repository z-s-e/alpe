/* Copyright 2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef LIBALPE_PIPEWIRE_H
#define LIBALPE_PIPEWIRE_H

#include "alpe.h"

#ifndef HAVE_ALPE_PIPEWIRE
#error ALPE_BUILD_PIPEWIRE is disabled
#endif

struct alpe_pipewire_lib_wrapper {
    alpe_pipewire_lib_wrapper();
    ~alpe_pipewire_lib_wrapper();
};

std::unique_ptr<alpe> alpe_create_pipewire(const char* app_name, const char* app_id, const char* icon_name);

#endif
