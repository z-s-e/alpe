/* Copyright 2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef LIBALPE_UTILS_H
#define LIBALPE_UTILS_H

#include "alpe.h"

#include <cmath>
#include <lbu/poll.h>
#include <sys/timerfd.h>


namespace alpe_utils {

constexpr int SteepFadeExpFactor = -15; // this equates to a fade low-point of approx. -90dB


inline bool required_buffer_too_large(const alpe::configuration& cfg, size_t frame_byte_size)
{
    return ((alpe::MaximumBufferSize / (cfg.buffer_count() * frame_byte_size)) / cfg.period_frames) == 0;
}


inline void set_timerfd_absolute(int fd, timespec t)
{
    itimerspec its = {};
    its.it_value = t;
    if( timerfd_settime(fd, TFD_TIMER_ABSTIME, &its, nullptr) != 0 )
        lbu::unexpected_system_error(errno);
}

inline void clear_timerfd(int fd)
{
    itimerspec its = {};
    if( timerfd_settime(fd, 0, &its, nullptr) != 0 )
        lbu::unexpected_system_error(errno);
}


inline bool has_revent(lbu::array_ref<const pollfd> pfds)
{
    for( const auto& p : pfds ) {
        if( p.revents )
            return true;
    }
    return false;
}


inline double amp_for_fade_in_progress(double progress, alpe::fade_parameter::Type fade_type)
{
    assert(progress >= 0.0 && progress <= 1.0);
    switch( fade_type ) {
    case alpe::fade_parameter::Type::Linear:
        return progress;
    case alpe::fade_parameter::Type::Moderate:
        return (progress * progress);
    case alpe::fade_parameter::Type::Steep:
        return std::exp2(SteepFadeExpFactor * (1.0 - progress));
    }
    return 0.5;
}

inline double progress_for_fade_in_amp(double amp, alpe::fade_parameter::Type fade_type)
{
    assert(amp >= 0.0 && amp <= 1.0);
    switch( fade_type ) {
    case alpe::fade_parameter::Type::Linear:
        return amp;
    case alpe::fade_parameter::Type::Moderate:
        return std::sqrt(amp);
    case alpe::fade_parameter::Type::Steep:
        return 1.0 - std::min((std::log2(amp) / SteepFadeExpFactor), 1.0);
    }
    return 0.5;
}

void apply_fade(char* dst, const char* src, lbu::alsa::pcm_format format, size_t frame_byte_size,
                size_t sample_byte_size, double factor,
                bool is_fade_in, alpe::fade_parameter::Type fade_type, alpe::short_duration_frames fade_duration,
                alpe::short_duration_frames fade_offset, alpe::short_duration_frames count);

inline void apply_fade(char* dst, const char* ring_base, alpe::short_duration_frames ring_offset, alpe::short_duration_frames ring_size,
                       lbu::alsa::pcm_format format, size_t frame_byte_size,
                       bool is_fade_in, alpe::fade_parameter::Type fade_type, alpe::short_duration_frames fade_duration,
                       alpe::short_duration_frames fade_offset, alpe::short_duration_frames count)
{
    const size_t sample_byte_size = lbu::alsa::pcm_format::packed_byte_size(format.type);
    const double factor = std::exp2(snd_pcm_format_width(format.type) - 1);
    const auto ring_first = ring_size - ring_offset;
    const bool wrap_ring = ring_first < count;
    apply_fade(dst, ring_base + (ring_offset * frame_byte_size), format, frame_byte_size, sample_byte_size, factor,
               is_fade_in, fade_type, fade_duration, fade_offset, wrap_ring ? ring_first : count);
    if( wrap_ring ) {
        apply_fade(dst + (ring_first * frame_byte_size), ring_base, format, frame_byte_size, sample_byte_size, factor,
                   is_fade_in, fade_type, fade_duration, fade_offset + ring_first, count - ring_first);
    }
}

inline double convert_fade_progress(double old_progress,
                                    alpe::fade_parameter::Type old_fade_type,
                                    bool old_is_fade_in,
                                    alpe::fade_parameter::Type new_fade_type,
                                    bool new_is_fade_in)
{
    const double amp = amp_for_fade_in_progress(old_is_fade_in ? old_progress : 1.0 - old_progress, old_fade_type);
    const double tmp = progress_for_fade_in_amp(amp, new_fade_type);
    return new_is_fade_in ? tmp : 1.0 - tmp;
}

}

#endif
