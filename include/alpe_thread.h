/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef LIBALPE_THREAD_H
#define LIBALPE_THREAD_H

#include "alpe.h"


class alpe_thread {
public:

    /// This application interface provides the required callbacks for the thread
    ///
    /// Note that all methods are called from the internal thread. Methods that are called
    /// during playback (request_buffer, buffer_ready) should not block.
    struct interface {
        struct reset_data {
            alpe* instance = {};
            pollfd buffer_ready_pollfd = {};
            alpe::fade_parameter post_underrun_fade_in;
        };

        struct buffer_ready_info {
            alpe::short_duration_frames frames = 0;
            alpe::EndOfStream eos = alpe::EndOfStream::No;
            bool error = false;
        };

        virtual reset_data reset() = 0;
        virtual void request_buffer(lbu::array_ref<void>) = 0;
        virtual buffer_ready_info buffer_ready() = 0;
        virtual alpe::long_duration_frames seek(alpe::long_duration_frames) = 0;
        virtual void report_underrun(alpe::playback_underrun_event) = 0;
        virtual void report_error(alpe::error_event) = 0;
    };

    enum class State {
        Initial,
        CommandPending,
        Paused,
        Buffering,
        Playing,
        Seeking,
        FadingOut,
        Resetting
    };


    explicit alpe_thread(interface* application_interface, alpe::debug_logger* logger = nullptr);

    pollfd event_fd() const;
    void set_playback_position_update_wakeup(bool wakeup);

    State state() const;
    alpe::playback_event last_playback_position();

    void reset(alpe::fade_parameter fade_out);
    void reset_autoplay(alpe::fade_parameter fade_out);
    void play(alpe::fade_parameter fade_in);
    void pause(alpe::fade_parameter fade_out);
    void seek(alpe::long_duration_frames pos, alpe::fade_parameter fade_out, alpe::fade_parameter fade_in);
    void seek_autoplay(alpe::long_duration_frames pos, alpe::fade_parameter fade_out, alpe::fade_parameter fade_in);
    void stop(alpe::fade_parameter fade_out);

    void wait_for_playback_finish();

    static int current_thread_set_realtime_priority();

    ~alpe_thread();
    alpe_thread(const alpe_thread&) = delete;
    alpe_thread& operator=(const alpe_thread&) = delete;

private:
    struct data;
    data* d = {};
};

#endif
