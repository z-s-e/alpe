/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef LIBALPE_H
#define LIBALPE_H

#include <chrono>
#include <lbu/alsa/alsa.h>
#include <lbu/math.h>
#include <optional>
#include <poll.h>
#include <variant>

namespace lbu::stream {
    class abstract_input_stream;
    class abstract_output_stream;
    class fd_input_stream;
}


class alpe {
public:
    enum class State {
        Unconfigured,
        Ready,
        Playing,
        Error
    };

    enum class EndOfStream { No, Yes };

    static constexpr auto MinimumPeriodTime = std::chrono::milliseconds(100);
    static constexpr auto MaximumPeriodTime = std::chrono::seconds(10);
    static constexpr auto MaximumSampleRate = 1000000;
    static constexpr auto MaximumBufferSize = 1024 * 1024 * 1024;

    using short_duration_frames = uint32_t;
    using long_duration_frames = uint64_t;

    struct fade_parameter {
        enum class Type {
            Linear,
            Moderate,
            Steep
        };
        Type type = Type::Linear;
        short_duration_frames duration = 0;
    };

    struct engine_parameter {
        uint8_t additional_buffer_count = 0;
        short_duration_frames pre_underrun_trigger_frames = 0;
        short_duration_frames rewind_safeguard = 0;
        short_duration_frames emergency_pause_fade = 0;
    };

    struct configuration {
        lbu::alsa::pcm_format format;
        short_duration_frames period_frames = 0;
        uint8_t period_buffer_count = 0;
        engine_parameter parameter;

        unsigned buffer_count() const { return unsigned(period_buffer_count) + parameter.additional_buffer_count; }
    };

    struct no_event {};

    struct playback_event {
        long_duration_frames position = 0;
        std::chrono::steady_clock::time_point time;
    };

    struct playback_stop_event {
        long_duration_frames position = 0;
    };

    struct playback_underrun_event {
        long_duration_frames position = 0;
        bool hard_underrun = false;
    };

    struct error_event {
        enum class Type {
            Alsa,
            Disconnected,
            Protocol,
            UnsupportedLowLatencyHardware,
            Api,
            Internal
        };
        Type type;
        int alsa_errno = 0;
    };

    using configuration_result = std::variant<error_event, configuration>;
    using event = std::variant<no_event, playback_event, playback_stop_event, playback_underrun_event, error_event>;
    using command_error = std::optional<error_event>;

    static std::unique_ptr<alpe> create_alsa(snd_pcm_t* pcm_device);
    static std::unique_ptr<alpe> create_stream(lbu::stream::abstract_input_stream* in_buffered_stream,
                                               pollfd in_stream_pollfd,
                                               lbu::stream::abstract_output_stream* out_buffered_stream);

    virtual lbu::array_ref<const pollfd> get_poll_fds() = 0;

    virtual bool has_buffered_events() = 0;
    virtual event next_event() = 0;

    virtual configuration_result configure(lbu::alsa::pcm_format requested_format,
                                           std::chrono::milliseconds requested_period_duration = std::chrono::seconds(1)) = 0;
    virtual configuration_result update_parameters(engine_parameter param) = 0;

    virtual command_error fade_in(fade_parameter param) = 0;
    virtual command_error fade_out(fade_parameter param) = 0;

    lbu::array_ref<void> get_buffer() { return current_buffer; }
    virtual command_error advance_buffer(short_duration_frames frames, EndOfStream eos = EndOfStream::No) = 0;
    virtual command_error drop_buffers() = 0;

    State state() const { return engine_state; }
    configuration current_configuration() const { return current_config; }
    long_duration_frames last_position() const { return last_playback_position; }
    short_duration_frames current_buffered_frames() const { return written_to_buffer - last_playback_position; }
    bool at_stream_end() const { return buffer_at_eos && last_playback_position == written_to_buffer; }

    virtual ~alpe() = default;

    static std::chrono::nanoseconds time_for_frames(short_duration_frames frames, uint32_t rate);
    static short_duration_frames frames_for_time(std::chrono::nanoseconds t, uint32_t rate);

    struct engine_parameter_system_preset {
        virtual engine_parameter generate(configuration current) = 0;
    };

    static void proxy_run(lbu::stream::fd_input_stream* in_stream, lbu::stream::abstract_output_stream* out_stream,
                          alpe* instance, pollfd interrupt_fd, engine_parameter_system_preset* preset = {});


    // debug interface
    enum class LogLevel { Info, Warning, Error };
    struct debug_logger {
        virtual void log(LogLevel level, const char* message);
        static lbu::array_ref<char> print_unix_time_day_clock(lbu::array_ref<char> dst); // dst should have size >= 13
    };
    void set_debug_logger(debug_logger* l) { logger = l; }

    static void log(debug_logger* l, LogLevel level, const char* fmt, ...) __attribute__ (( format( __printf__, 3, 4 )));

protected:
    State engine_state = State::Unconfigured;
    configuration current_config;
    lbu::array_ref<void> current_buffer;
    long_duration_frames last_playback_position = 0;
    long_duration_frames written_to_buffer = 0;
    bool buffer_at_eos = false;

    static debug_logger stderr_logger;
    debug_logger* logger = &stderr_logger;

    void log_info(const char* fmt, ...) const __attribute__ (( format( __printf__, 2, 3 )));
    void log_warning(const char* fmt, ...) const __attribute__ (( format( __printf__, 2, 3 )));
    void log_error(const char* fmt, ...) const __attribute__ (( format( __printf__, 2, 3 )));
    error_event log_api_error(const char* fmt, ...) const __attribute__ (( format( __printf__, 2, 3 )));

    friend struct alpe_proxy;
};


inline std::chrono::nanoseconds alpe::time_for_frames(short_duration_frames frames, uint32_t rate)
{
    return std::chrono::nanoseconds((uint64_t(frames) * std::nano::den) / rate);
}

inline alpe::short_duration_frames alpe::frames_for_time(std::chrono::nanoseconds t, uint32_t rate)
{
    assert(t.count() >= 0 && uint64_t(t.count()) <= std::numeric_limits<uint64_t>::max() / rate);
    auto r = lbu::idiv_ceil(uint64_t(t.count()) * rate, uint64_t(std::nano::den));
    assert(r <= std::numeric_limits<int32_t>::max());
    return r;
}

#endif
