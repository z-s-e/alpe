/* Copyright 2025 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "alpe_pipewire.h"

#include "alpe_utils.h"

#include <lbu/ring_spsc.h>
#include <lbu/triple_buffer_spsc.h>
#include <pipewire/pipewire.h>
#include <spa/param/audio/format-utils.h>
#include <spa/utils/result.h>
#include <string>


constexpr auto DefaultUnderrunFade = std::chrono::milliseconds(10);
constexpr auto TimingInfoRecheckWaitTime = std::chrono::milliseconds(50);
constexpr unsigned PipeWirePeriodCount = 2;

using steady_clock = std::chrono::steady_clock;
using ring = lbu::ring_spsc::algorithm::mirrored_index<alpe::short_duration_frames>;


namespace {

enum class FadeMode { NoFade, In, Out, EmergencyOut };

struct pipewire_timing_info {
    int64_t frame_position = -1;
    steady_clock::time_point timestamp;
};

struct next_fade_command {
    FadeMode mode = FadeMode::NoFade;
    alpe::fade_parameter param;
};

struct fade_progress {
    FadeMode mode = FadeMode::NoFade;
    alpe::fade_parameter param;
    alpe::long_duration_frames end = 0;
};

struct pipewire_callback_data {
    pw_stream* stream = {};

    // on_process accessed data, needs to be RT safe if also accessed from main thread while stream is active
    lbu::alsa::pcm_format format;
    unsigned frame_byte_size = 0;
    alpe::short_duration_frames emergency_pause_fade = 0;

    alpe::short_duration_frames buffer_frame_count = 0;
    lbu::unique_cpod<char> buffer;

    fade_progress current_fade;

    alpe::long_duration_frames written_to_device = 0;

    lbu::triple_buffer_spsc<pipewire_timing_info> timing_info;
    lbu::triple_buffer_spsc<next_fade_command> fade_command;

    std::atomic<alpe::short_duration_frames> producer_idx;
    std::atomic<alpe::short_duration_frames> eos_idx;
    std::atomic<alpe::short_duration_frames> consumer_idx;

    // on_state_changed data, main thread only
    bool has_next_state = false;
    pw_stream_state next_state = PW_STREAM_STATE_UNCONNECTED;
    std::string next_error;
};


static alpe::short_duration_frames minimum_buffer_free_frames(alpe::short_duration_frames period_size) { return period_size / 10; }

static spa_audio_format alsa_format_to_pipewire(snd_pcm_format_t format)
{
    switch( format ) {
    case SND_PCM_FORMAT_S8:             return SPA_AUDIO_FORMAT_S8;
    case SND_PCM_FORMAT_S16_LE:         return SPA_AUDIO_FORMAT_S16_LE;
    case SND_PCM_FORMAT_S16_BE:         return SPA_AUDIO_FORMAT_S16_BE;
    case SND_PCM_FORMAT_S18_3LE:        return SPA_AUDIO_FORMAT_S18_LE;
    case SND_PCM_FORMAT_S18_3BE:        return SPA_AUDIO_FORMAT_S18_BE;
    case SND_PCM_FORMAT_S20_3LE:        return SPA_AUDIO_FORMAT_S20_LE;
    case SND_PCM_FORMAT_S20_3BE:        return SPA_AUDIO_FORMAT_S20_BE;
    case SND_PCM_FORMAT_S24_LE:         return SPA_AUDIO_FORMAT_S24_32_LE;
    case SND_PCM_FORMAT_S24_BE:         return SPA_AUDIO_FORMAT_S24_32_BE;
    case SND_PCM_FORMAT_S24_3LE:        return SPA_AUDIO_FORMAT_S24_LE;
    case SND_PCM_FORMAT_S24_3BE:        return SPA_AUDIO_FORMAT_S24_BE;
    case SND_PCM_FORMAT_S32_LE:         return SPA_AUDIO_FORMAT_S32_LE;
    case SND_PCM_FORMAT_S32_BE:         return SPA_AUDIO_FORMAT_S32_BE;
    case SND_PCM_FORMAT_FLOAT_LE:       return SPA_AUDIO_FORMAT_F32_LE;
    case SND_PCM_FORMAT_FLOAT_BE:       return SPA_AUDIO_FORMAT_F32_BE;
    case SND_PCM_FORMAT_FLOAT64_LE:     return SPA_AUDIO_FORMAT_F64_LE;
    case SND_PCM_FORMAT_FLOAT64_BE:     return SPA_AUDIO_FORMAT_F64_BE;
    default:                            return SPA_AUDIO_FORMAT_UNKNOWN;
    }
}

static fade_progress update_fade_progress(fade_progress previous, next_fade_command next, alpe::long_duration_frames written_to_device)
{
    double current_fade_progress = 1.0;
    if( previous.mode != FadeMode::NoFade )
        current_fade_progress = std::max(0.0, 1.0 - double(previous.end - written_to_device) / previous.param.duration);
    const auto new_fade_progress = alpe_utils::convert_fade_progress(current_fade_progress, previous.param.type, previous.mode != FadeMode::Out,
                                                                     next.param.type, next.mode == FadeMode::In);
    const auto fade_end = written_to_device + std::rint((1.0 - new_fade_progress) * next.param.duration);
    return {next.mode, next.param, alpe::long_duration_frames(fade_end)};
}

static fade_progress current_fade_progress(alpe::long_duration_frames written_to_device,
                                           alpe::short_duration_frames available,
                                           unsigned requested,
                                           pipewire_callback_data* cb)
{
    fade_progress result = cb->current_fade;
    if( result.mode == FadeMode::EmergencyOut )
        return result;

    cb->fade_command.swap_consumer_buffer();
    if( const auto next_fade = cb->fade_command.current_consumer_buffer(); next_fade.mode != FadeMode::NoFade ) {
        result = update_fade_progress(result, next_fade, written_to_device);
        cb->fade_command.current_consumer_buffer().mode = FadeMode::NoFade;
    }

    const auto emergency_pause_fade = cb->emergency_pause_fade;
    if( available >= requested + emergency_pause_fade )
        return result;
    if( result.mode == FadeMode::Out && result.end - written_to_device <= std::min(available, requested) )
        return result;
    return update_fade_progress(result, {FadeMode::EmergencyOut, {alpe::fade_parameter::Type::Linear, emergency_pause_fade}}, written_to_device);
}


static void on_process(void* userdata)
{
    auto cb = static_cast<pipewire_callback_data *>(userdata);

    auto stream = cb->stream;
    const auto stream_format = cb->format;
    const auto frame_byte_size = cb->frame_byte_size;
    const auto ring_size = cb->buffer_frame_count;
    const char* ring_base_ptr = cb->buffer.get();
    const auto written_to_device = cb->written_to_device;

    { // update timing info
        pw_time t;
        pw_stream_get_time_n(stream, &t, sizeof(t));

        int64_t hw_pos = int64_t(written_to_device)
                         - int64_t(t.queued + t.buffered)
                         - (t.delay * stream_format.rate * t.rate.num / t.rate.denom);

        cb->timing_info.current_producer_buffer() = {hw_pos, steady_clock::time_point(std::chrono::nanoseconds(t.now))};
        cb->timing_info.swap_producer_buffer();
    }

    auto pw_buf = pw_stream_dequeue_buffer(stream);
    if( pw_buf == nullptr )
        return;
    const auto pw_buf_data = pw_buf->buffer->datas[0];
    auto dst_chunk = pw_buf_data.chunk;
    auto dst_ptr = static_cast<char*>(pw_buf_data.data);
    assert(dst_ptr && dst_chunk);

    auto requested = pw_buf_data.maxsize / frame_byte_size;
    if( pw_buf->requested )
        requested = std::min<unsigned>(requested, pw_buf->requested);

    const auto consumer_idx = cb->consumer_idx.load(std::memory_order_relaxed);
    const auto producer_idx = cb->producer_idx.load(std::memory_order_acquire);
    const bool producer_at_end = (cb->eos_idx.load(std::memory_order_acquire) == producer_idx);

    const auto available = ring::consumer_slots(producer_idx, consumer_idx, ring_size);
    const auto fade = current_fade_progress(written_to_device,
                                            producer_at_end ? std::numeric_limits<alpe::short_duration_frames>::max() : available,
                                            requested, cb);

    alpe::short_duration_frames frames_to_write = std::min(available, requested);
    alpe::short_duration_frames consumer_offset = ring::offset(consumer_idx, ring_size);
    bool do_drain = (producer_at_end && available <= requested);
    alpe::short_duration_frames frames_written = 0;

    if( fade.mode != FadeMode::NoFade ) {
        const alpe::short_duration_frames fade_frames = fade.end - written_to_device;
        const bool is_fade_in = (fade.mode == FadeMode::In);
        const auto fade_to_write = std::min(frames_to_write, fade_frames);

        alpe_utils::apply_fade(dst_ptr, ring_base_ptr, consumer_offset, ring_size,
                               stream_format, frame_byte_size, is_fade_in, fade.param.type, fade.param.duration,
                               fade.param.duration - (fade.end - written_to_device), fade_to_write);
        frames_written = fade_to_write;

        if( frames_written == fade_frames ) {
            if( fade.mode == FadeMode::In ) {
                cb->current_fade.mode = FadeMode::NoFade;
                frames_to_write -= frames_written;
                consumer_offset = ring::offset(ring::new_index(consumer_idx, frames_written, ring_size), ring_size);
            } else { // fade out or emergency out
                cb->current_fade = fade;
                frames_to_write = 0;
                do_drain = true;
            }
        } else {
            cb->current_fade = fade;
            frames_to_write = 0;
            if( fade.mode == FadeMode::EmergencyOut && frames_written < requested )
                do_drain = true;
        }
    }

    { // copy regular (non-faded) frames
        const auto first_copy = std::min(frames_to_write, ring_size - consumer_offset);
        const auto second_copy = (frames_to_write - first_copy);
        std::memcpy(dst_ptr + (frames_written * frame_byte_size), ring_base_ptr + (consumer_offset * frame_byte_size),
                    first_copy * frame_byte_size);
        frames_written += first_copy;
        if( second_copy > 0 ) {
            std::memcpy(dst_ptr + (frames_written * frame_byte_size), ring_base_ptr, second_copy * frame_byte_size);
            frames_written += second_copy;
        }
    }

    dst_chunk->offset = 0;
    dst_chunk->stride = int(frame_byte_size);
    dst_chunk->size = frames_written * frame_byte_size;

    cb->written_to_device += frames_written;
    cb->consumer_idx.store(ring::new_index(consumer_idx, frames_written, ring_size), std::memory_order_release);

    pw_stream_queue_buffer(stream, pw_buf);

    if( do_drain )
        pw_stream_flush(stream, true);
}

static void on_state_changed(void* userdata, pw_stream_state, pw_stream_state state, const char* error)
{
    auto cb = static_cast<pipewire_callback_data *>(userdata);
    cb->has_next_state = true;
    cb->next_state = state;
    if( state == PW_STREAM_STATE_ERROR )
        cb->next_error = error;
}

static void on_drained(void* userdata)
{
    pw_stream_set_active(static_cast<pipewire_callback_data *>(userdata)->stream, false);
}

static const struct pw_stream_events StreamEvents = {
    PW_VERSION_STREAM_EVENTS,
    {}, // void (*destroy) (void *data);
    on_state_changed, // void (*state_changed) (void *data, pw_stream_state old, pw_stream_state state, const char *error);
    {}, // void (*control_info) (void *data, uint32_t id, const struct pw_stream_control *control);
    {}, // void (*io_changed) (void *data, uint32_t id, void *area, uint32_t size);
    {}, // void (*param_changed) (void *data, uint32_t id, const struct spa_pod *param);
    {}, // void (*add_buffer) (void *data, struct pw_buffer *buffer);
    {}, // void (*remove_buffer) (void *data, struct pw_buffer *buffer);
    on_process, // void (*process) (void *data);
    on_drained, // void (*drained) (void *data);
    {}, // void (*command) (void *data, const struct spa_command *command);
    {}, // void (*trigger_done) (void *data);
};

} // namespace


struct alpe_pipewire : public alpe {
    enum PollFdIndex {
        PollFdPWLoop,
        PollFdTimer
    };

    pw_main_loop* pipewire_main_loop = {};
    pollfd pfds[2] = {};
    steady_clock::time_point next_timer_wakeup;
    std::chrono::milliseconds timer_increment;
    pipewire_callback_data cb;


    static configuration default_configuration(lbu::alsa::pcm_format format, std::chrono::milliseconds period_duration)
    {
        configuration cfg;
        cfg.format = format;
        cfg.period_frames = frames_for_time(period_duration, format.rate);
        cfg.period_buffer_count = PipeWirePeriodCount;
        cfg.parameter.additional_buffer_count = 1;
        cfg.parameter.emergency_pause_fade = frames_for_time(DefaultUnderrunFade, format.rate);
        return cfg;
    }

    void set_timer(steady_clock::time_point t)
    {
        next_timer_wakeup = t;
        alpe_utils::set_timerfd_absolute(pfds[PollFdTimer].fd, lbu::poll::timespec_for_duration(t.time_since_epoch()));
    }

    void clear_timer()
    {
        next_timer_wakeup = {};
        alpe_utils::clear_timerfd(pfds[PollFdTimer].fd);
    }

    error_event set_error(error_event e)
    {
        engine_state = State::Error;
        current_buffer = {};
        clear_timer();
        return e;
    }

    error_event set_error_pw_stream_error_or_disconnected()
    {
        if( cb.next_state == PW_STREAM_STATE_ERROR )
            log_error("PipeWire error: %s", cb.next_error.c_str());
        else
            log_error("PipeWire disconnected");
        return set_error({error_event::Type::Disconnected});
    }

    error_event set_error_unexpected_pw_state()
    {
        log_error("PipeWire unexpected state %s", pw_stream_state_as_string(cb.next_state));
        return set_error({error_event::Type::Internal});
    }

    void clear_buffer()
    {
        last_playback_position = 0;
        written_to_buffer = 0;
        buffer_at_eos = false;
        cb.written_to_device = 0;
        cb.producer_idx = 0;
        cb.eos_idx = std::numeric_limits<short_duration_frames>::max();
        cb.consumer_idx = 0;

        current_buffer = lbu::array_ref<char>(cb.buffer.get(), cb.buffer_frame_count * cb.frame_byte_size);
    }

    void update_current_buffer()
    {
        const auto producer_idx = cb.producer_idx.load(std::memory_order_relaxed);
        const auto consumer_idx = cb.consumer_idx.load(std::memory_order_acquire);
        const auto ring_size = cb.buffer_frame_count;
        const auto ring_avail = ring::producer_slots(producer_idx, consumer_idx, ring_size);

        if( buffer_at_eos || (ring_avail < minimum_buffer_free_frames(current_config.period_frames)) ) {
            current_buffer = {};
            return;
        }
        const auto offset = ring::offset(producer_idx, ring_size);
        const auto frame_size = cb.frame_byte_size;
        const auto continuous = lbu::ring_spsc::algorithm::continuous_slots(offset, ring_avail, ring_size);
        current_buffer = lbu::array_ref<char>(cb.buffer.get() + (offset * frame_size), continuous * frame_size);
    }

    configuration_result set_current_config(const configuration& cfg)
    {
        current_config = cfg;
        cb.format = cfg.format;
        cb.frame_byte_size = lbu::alsa::pcm_format::frame_packed_byte_size(cfg.format.type, cfg.format.channels);
        cb.emergency_pause_fade = cfg.parameter.emergency_pause_fade;

        if( alpe_utils::required_buffer_too_large(current_config, cb.frame_byte_size) ) {
            log_error("required buffer too large for given configuration and parameter");
            return set_error({error_event::Type::Internal});
        }

        const unsigned buffer_count = PipeWirePeriodCount + cfg.parameter.additional_buffer_count;
        cb.buffer_frame_count = buffer_count * cfg.period_frames;
        cb.buffer.reset(lbu::xmalloc_bytes<char>(cb.buffer_frame_count * cb.frame_byte_size));
        clear_buffer();

        return current_config;
    }

    void set_paused()
    {
        cb.timing_info.swap_consumer_buffer();
        cb.timing_info.current_consumer_buffer().frame_position = -1;
        cb.fade_command.current_producer_buffer().mode = FadeMode::NoFade;
        cb.fade_command.swap_producer_buffer();
        clear_timer();
        engine_state = State::Ready;
    }

    event handle_stop()
    {
        set_paused();

        const auto pos = cb.written_to_device;
        const bool at_stop = (cb.current_fade.mode == FadeMode::Out && pos == cb.current_fade.end);
        const bool at_emergency_stop = (cb.current_fade.mode == FadeMode::EmergencyOut && pos == cb.current_fade.end);

        last_playback_position = pos;
        update_current_buffer();

        if( at_stream_end() || at_stop )
            return playback_stop_event{ pos };
        return playback_underrun_event{ pos, ! at_emergency_stop };
    }

    bool pipewire_loop_iterate(int timeout = 0)
    {
        if( int result = pw_loop_iterate(pw_main_loop_get_loop(pipewire_main_loop), timeout); result < 0 ) {
            cb.has_next_state = true;
            cb.next_state = PW_STREAM_STATE_ERROR;
            cb.next_error = std::string("loop iterate failed - ") + spa_strerror(result);
        }
        return cb.has_next_state;
    }


    lbu::array_ref<const pollfd> get_poll_fds() override { return lbu::array_ref<const pollfd>(pfds); }

    bool has_buffered_events() override { return false; }

    event next_event() override
    {
        if( next_timer_wakeup != steady_clock::time_point() && steady_clock::now() >= next_timer_wakeup ) {
            assert(engine_state == State::Playing);
            cb.timing_info.swap_consumer_buffer();
            if( auto& t = cb.timing_info.current_consumer_buffer(); t.frame_position >= 0 ) {
                playback_event p{ long_duration_frames(t.frame_position), t.timestamp };
                last_playback_position = p.position;
                update_current_buffer();
                set_timer(p.time + timer_increment);
                t.frame_position = -1;
                return p;
            }

            set_timer(steady_clock::now() + TimingInfoRecheckWaitTime);
        } else if( engine_state == State::Error ) {
            return log_api_error("next_event called while already in error state");
        }

        if( ! pipewire_loop_iterate() )
            return no_event{};

        cb.has_next_state = false;
        switch( cb.next_state ) {
        case PW_STREAM_STATE_ERROR:
        case PW_STREAM_STATE_UNCONNECTED:
        case PW_STREAM_STATE_CONNECTING:
            return set_error_pw_stream_error_or_disconnected();
        case PW_STREAM_STATE_PAUSED:
            if( engine_state != State::Playing )
                return set_error_unexpected_pw_state();
            return handle_stop();
        case PW_STREAM_STATE_STREAMING:
        default:
            return set_error_unexpected_pw_state();
        }
    }

    configuration_result configure(lbu::alsa::pcm_format requested_format, std::chrono::milliseconds requested_period_duration) override
    {
        if( engine_state != State::Unconfigured && engine_state != State::Ready )
            return log_api_error("configure called while not ready");
        const auto pipewire_format = alsa_format_to_pipewire(requested_format.type);
        if( pipewire_format == SPA_AUDIO_FORMAT_UNKNOWN )
            return log_api_error("only signed linear pcm formats supported");
        if( requested_format.channels == 0 )
            return log_api_error("zero channels requested");
        if( requested_format.rate > MaximumSampleRate )
            return log_api_error("unsupported large sample rate %u", requested_format.rate);
        if( requested_period_duration < MinimumPeriodTime || requested_period_duration > MaximumPeriodTime )
            return log_api_error("requested_period_duration %ld out of range", long(requested_period_duration.count()));

        const auto cfg = default_configuration(requested_format, requested_period_duration);

        log_info("configure format %s, %u channels, %u Hz, %u ms period duration (%u frames); "
                 "parameter: %u additional buffer, %u emergency pause fade",
                 snd_pcm_format_name(requested_format.type), requested_format.channels, requested_format.rate,
                 unsigned(requested_period_duration.count()), cfg.period_frames,
                 cfg.parameter.additional_buffer_count, cfg.parameter.emergency_pause_fade);

        {
            const spa_pod* params[1];
            uint8_t buffer[1024];
            spa_pod_builder b{ buffer, sizeof(buffer), 0, {}, {} };

            spa_audio_info_raw tmp{ pipewire_format, 0, requested_format.rate, requested_format.channels, {} };
            params[0] = spa_format_audio_raw_build(&b, SPA_PARAM_EnumFormat, &tmp);
            pw_stream_update_params(cb.stream, params, 1);
        }

        timer_increment = requested_period_duration;
        engine_state = State::Ready;

        return set_current_config(cfg);
    }

    configuration_result update_parameters(engine_parameter param) override
    {
        if( engine_state != State::Ready )
            return log_api_error("update_parameters called while not in ready state");
        if( param.emergency_pause_fade > (current_config.period_frames * (param.additional_buffer_count + PipeWirePeriodCount)) )
            return log_api_error("Emergency pause fade larger than buffer");

        current_config.parameter = param;

        log_info("updated parameter to %u additional buffer, %u emergency pause fade",
                 param.additional_buffer_count, param.emergency_pause_fade);

        return set_current_config(current_config);
    }

    command_error fade_in(fade_parameter param) override
    {
        if( engine_state == State::Playing ) {
            cb.fade_command.current_producer_buffer() = {FadeMode::In, param};
            cb.fade_command.swap_producer_buffer();
            return {};
        } else if( engine_state != State::Ready ) {
            return log_api_error("fade_in called while not ready");
        }

        cb.current_fade.mode = param.duration == 0 ? FadeMode::NoFade : FadeMode::In;
        cb.current_fade.param = param;
        cb.current_fade.end = last_playback_position + param.duration;

        pw_stream_set_active(cb.stream, true);

        while( ! pipewire_loop_iterate(-1) )
            ;

        cb.has_next_state = false;
        switch( cb.next_state ) {
        case PW_STREAM_STATE_ERROR:
        case PW_STREAM_STATE_UNCONNECTED:
        case PW_STREAM_STATE_CONNECTING:
            return set_error_pw_stream_error_or_disconnected();
        case PW_STREAM_STATE_STREAMING:
            engine_state = State::Playing;
            set_timer(steady_clock::now() + TimingInfoRecheckWaitTime); // timing info will take a while to be set, so wait a bit before checking
            return {};
        case PW_STREAM_STATE_PAUSED:
        default:
            return set_error_unexpected_pw_state();
        }
    }

    command_error fade_out(fade_parameter param) override
    {
        if( engine_state != State::Playing )
            return log_api_error("fade_out called while not playing");

        cb.fade_command.current_producer_buffer() = {FadeMode::Out, param};
        cb.fade_command.swap_producer_buffer();
        return {};
    }

    command_error advance_buffer(short_duration_frames frames, EndOfStream eos) override
    {
        if( current_buffer.byte_size() < frames * cb.frame_byte_size )
            return log_api_error("cannot advance buffer beyond its size");

        written_to_buffer += frames;
        buffer_at_eos = (eos == EndOfStream::Yes);

        const auto ring_size = cb.buffer_frame_count;
        const auto producer_idx = cb.producer_idx.load(std::memory_order_relaxed);
        const auto new_idx = ring::new_index(producer_idx, frames, ring_size);
        if( buffer_at_eos )
            cb.eos_idx.store(new_idx, std::memory_order_release);
        cb.producer_idx.store(new_idx, std::memory_order_release);

        update_current_buffer();
        return {};
    }

    command_error drop_buffers() override
    {
        if( engine_state == State::Playing ) {
            pw_stream_set_active(cb.stream, false);

            while( ! pipewire_loop_iterate(-1) )
                ;

            cb.has_next_state = false;
            switch( cb.next_state ) {
            case PW_STREAM_STATE_ERROR:
            case PW_STREAM_STATE_UNCONNECTED:
            case PW_STREAM_STATE_CONNECTING:
                return set_error_pw_stream_error_or_disconnected();
            case PW_STREAM_STATE_PAUSED:
                set_paused();
                break;
            case PW_STREAM_STATE_STREAMING:
            default:
                return set_error_unexpected_pw_state();
            }
        }

        if( engine_state != State::Ready )
            return log_api_error("drop_buffers called while not ready");

        clear_buffer();
        return {};
    }

    ~alpe_pipewire() override
    {
        if( cb.stream ) {
            lbu::fd(pfds[PollFdTimer].fd).close();
            pw_stream_destroy(cb.stream);
            pw_loop_leave(pw_main_loop_get_loop(pipewire_main_loop));
        }
        if( pipewire_main_loop )
            pw_main_loop_destroy(pipewire_main_loop);
    }

    alpe_pipewire(const char* app_name, const char* app_id, const char* icon_name)
    {
        engine_state = State::Error; // assume init failure, change to Unconfigured when init complete

        pipewire_main_loop = pw_main_loop_new(nullptr);
        if( pipewire_main_loop == nullptr )
            return;
        pw_loop* loop = pw_main_loop_get_loop(pipewire_main_loop);

        {
            static const char playback[] = "Playback";
            auto props = pw_properties_new(PW_KEY_MEDIA_TYPE, "Audio",
                                           PW_KEY_MEDIA_CATEGORY, playback,
                                           PW_KEY_MEDIA_ROLE, "Music",
                                           PW_KEY_APP_ID, app_id,
                                           PW_KEY_APP_ICON_NAME, icon_name,
                                           PW_KEY_APP_NAME, app_name,
                                           nullptr);
            cb.stream = pw_stream_new_simple(loop, playback, props, &StreamEvents, &cb);
        }
        if( cb.stream == nullptr )
            return;

        {
            const spa_pod* params[1];
            uint8_t buffer[1024];
            spa_pod_builder b{ buffer, sizeof(buffer), 0, {}, {} };
            spa_audio_info_raw tmp{ SPA_AUDIO_FORMAT_F32, 0, 48000, 2, {} };
            params[0] = spa_format_audio_raw_build(&b, SPA_PARAM_EnumFormat, &tmp);
            int flags = PW_STREAM_FLAG_INACTIVE | PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_MAP_BUFFERS | PW_STREAM_FLAG_RT_PROCESS;

            pw_stream_connect(cb.stream, PW_DIRECTION_OUTPUT, PW_ID_ANY, static_cast<pw_stream_flags>(flags), params, 1);
        }

        pfds[PollFdPWLoop] = pollfd{ pw_loop_get_fd(loop), POLLIN, 0 };
        pfds[PollFdTimer] = pollfd{ timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK|TFD_CLOEXEC), POLLIN, 0 };
        if( ! lbu::fd(pfds[PollFdTimer].fd) )
            lbu::unexpected_system_error(errno);

        pw_loop_enter(loop);

        for( int retry = 0; retry < 5; ++retry ) {
            if( ! pipewire_loop_iterate(1000) )
                continue;

            cb.has_next_state = false;
            switch( cb.next_state ) {
            case PW_STREAM_STATE_ERROR:
                set_error_pw_stream_error_or_disconnected();
                return;
            case PW_STREAM_STATE_CONNECTING:
                continue;
            case PW_STREAM_STATE_PAUSED:
                engine_state = State::Unconfigured;
                return;
            case PW_STREAM_STATE_UNCONNECTED:
            case PW_STREAM_STATE_STREAMING:
            default:
                set_error_unexpected_pw_state();
                return;
            }
        }
        log_warning("PipeWire wait for stream connection timed out");
    }
};

alpe_pipewire_lib_wrapper::alpe_pipewire_lib_wrapper() { pw_init(nullptr, nullptr); }
alpe_pipewire_lib_wrapper::~alpe_pipewire_lib_wrapper() { pw_deinit(); }

std::unique_ptr<alpe> alpe_create_pipewire(const char* app_name, const char* app_id, const char* icon_name)
{
    return std::unique_ptr<alpe>(new alpe_pipewire(app_name, app_id, icon_name));
}
