/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "alpe_utils.h"

#include <lbu/io.h>


//#define ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE 1

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
#include <stdio.h>
static const char TestFile[] = "/tmp/alpe-alsa-testout.raw";
#endif

constexpr auto MaximumPeriodFrames = alpe::MaximumSampleRate * std::chrono::seconds(alpe::MaximumPeriodTime).count();
constexpr auto MaximumAlsaPeriods = 50;
constexpr auto MinimumWaitTime = std::chrono::milliseconds(2);
constexpr unsigned MaximumClockDriftPercent = 1;
constexpr unsigned MinimumBufferFreePercent = 2;
constexpr unsigned FadeFillShortPercent = 5;
constexpr auto DefaultPreUnderrunTime = std::chrono::milliseconds(60);
constexpr auto DefaultRewindSafeguard = std::chrono::milliseconds(20);
constexpr auto DefaultUnderrunFade = std::chrono::milliseconds(10);

using steady_clock = std::chrono::steady_clock;


namespace alpe_utils {

void apply_fade(char* dst, const char* src, lbu::alsa::pcm_format format, size_t frame_byte_size,
                size_t sample_byte_size, double factor,
                bool is_fade_in, alpe::fade_parameter::Type fade_type, alpe::short_duration_frames fade_duration,
                alpe::short_duration_frames fade_offset, alpe::short_duration_frames count)
{
    assert(fade_duration >= count);
    for( alpe::short_duration_frames i = 0; i < count; ++i ) {
        const char* src_frame = src + (i * frame_byte_size);
        char* dst_frame = dst + (i * frame_byte_size);
        const double progress = double(fade_offset + i) / fade_duration;
        const double amp = amp_for_fade_in_progress(is_fade_in ? progress : 1.0 - progress, fade_type);
        assert(amp >= 0.0 && amp <= 1.0);

        for( uint16_t j = 0; j < format.channels; ++j ) {
            const char* src_chan = src_frame + j * sample_byte_size;
            char* dst_chan = dst_frame + j * sample_byte_size;
            using pcm = lbu::alsa::pcm_buffer;

            switch( format.type ) {
            case SND_PCM_FORMAT_FLOAT_LE:
                pcm::write_f(dst_chan, pcm::read_f(src_chan, SND_PCM_FORMAT_FLOAT_LE) * amp, SND_PCM_FORMAT_FLOAT_LE);
                break;
            case SND_PCM_FORMAT_FLOAT_BE:
                pcm::write_f(dst_chan, pcm::read_f(src_chan, SND_PCM_FORMAT_FLOAT_BE) * amp, SND_PCM_FORMAT_FLOAT_BE);
                break;
            case SND_PCM_FORMAT_FLOAT64_LE:
                pcm::write_d(dst_chan, pcm::read_d(src_chan, SND_PCM_FORMAT_FLOAT64_LE) * amp, SND_PCM_FORMAT_FLOAT64_LE);
                break;
            case SND_PCM_FORMAT_FLOAT64_BE:
                pcm::write_d(dst_chan, pcm::read_d(src_chan, SND_PCM_FORMAT_FLOAT64_BE) * amp, SND_PCM_FORMAT_FLOAT64_BE);
                break;
            default: {
                    double sample = (double(pcm::read_i32(src_chan, format.type)) / factor) * amp;
                    sample = std::clamp<double>(std::rint(sample * factor), -factor, factor - 1);
                    pcm::write_i32(dst_chan, sample, format.type);
                }
            }
        }
    }
}

} // namespace alpe_utils


static alpe::short_duration_frames percent_frames(alpe::short_duration_frames frames, unsigned percent)
{
    return lbu::idiv_ceil(uint64_t(frames) * percent, uint64_t(100));
}

static steady_clock::time_point status_htstamp(snd_pcm_status_t* status)
{
    snd_htimestamp_t ht;
    snd_pcm_status_get_htstamp(status, &ht);
    return steady_clock::time_point(std::chrono::seconds(ht.tv_sec) + std::chrono::nanoseconds(ht.tv_nsec));
}

static steady_clock::time_point status_trigger_htstamp(snd_pcm_status_t* status)
{
    snd_htimestamp_t ht;
    snd_pcm_status_get_trigger_htstamp(status, &ht);
    return steady_clock::time_point(std::chrono::seconds(ht.tv_sec) + std::chrono::nanoseconds(ht.tv_nsec));
}

constexpr alpe::error_event UnderrunErrorEvent{alpe::error_event::Type::Alsa, -EPIPE};
static bool is_underrun_error(int err) { return (err == -EPIPE || err == -ESTRPIPE); }
static bool is_underrun_error_event(alpe::error_event e) { return e.type == alpe::error_event::Type::Alsa && is_underrun_error(e.alsa_errno); }

static snd_pcm_sframes_t writei_silence(snd_pcm_t* pcm, size_t frame_byte_size, snd_pcm_uframes_t frames)
{
    static struct alignas(8) {
        char data[128 * 1024] = {};
    } ZeroBuffer; // this is conceptually const, but not marked as such, so that it can go into the bss section

    const snd_pcm_uframes_t zero_frames = sizeof(ZeroBuffer) / frame_byte_size;
    while( frames > 0 ) {
        const auto w = std::min(zero_frames, frames);
        auto res = snd_pcm_writei(pcm, ZeroBuffer.data, w);
        if( res < 0 )
            return res;
        if( snd_pcm_uframes_t(res) != w )
            return UnderrunErrorEvent.alsa_errno;
        frames -= w;
    }
    return 0;
}


struct alpe_alsa : public alpe {
    enum class FadeMode { In, Out, EmergencyOut };
    enum class FadeFill { Default, ShortFirst };

    snd_pcm_t* const pcm = {};

    lbu::alsa::pcm_device_status pcm_status;
    lbu::poll::unique_pollfd timer_pfd;

    size_t frame_byte_size = 0;
    short_duration_frames minimum_buffer_free_frames = 0;

    alpe::short_duration_frames buffer_frame_count = 0;
    lbu::unique_cpod<char> buffer; // holds buffer_frame_count plus another period_frames of scratch buffer at the end for fading

    alpe::long_duration_frames last_stop_position = 0;
    alpe::long_duration_frames written_to_device = 0;

    FadeMode fade_mode = FadeMode::In;
    fade_parameter fade_param;
    long_duration_frames fade_end_position = 0;


#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
    FILE* test_out = {};

    void write_test_data(const void* src, alpe::short_duration_frames frames) const {
        if( fwrite(src, frames * frame_byte_size, 1, test_out) != 1 )
            log_error("write test data failed");
    }

    void rewind_test_data(long rewind) const {
        if( fseek(test_out, -rewind * long(frame_byte_size), SEEK_CUR) != 0 )
            log_error("seek test data failed");
    }
#endif


    static alpe::engine_parameter default_engine_parameter(uint32_t sample_rate)
    {
        alpe::engine_parameter p;
        p.additional_buffer_count = 1;
        p.pre_underrun_trigger_frames = frames_for_time(DefaultPreUnderrunTime, sample_rate);
        p.rewind_safeguard = frames_for_time(DefaultRewindSafeguard, sample_rate);
        p.emergency_pause_fade = frames_for_time(DefaultUnderrunFade, sample_rate);
        return p;
    }

    steady_clock::time_point next_hw_period_wakeup(steady_clock::time_point last_update_time) const
    {
        const auto period_frames = current_config.period_frames;
        const auto frames_to_next_period = period_frames - ((last_playback_position - last_stop_position) % period_frames);
        auto wait_frames = percent_frames(frames_to_next_period, 100 + MaximumClockDriftPercent);

        // Optimization: when we are close to the next period and the hw buffer is already nearly full, we can wait another period
        const auto hw_frames = current_config.period_buffer_count * period_frames;
        const auto hw_avail = hw_frames - (written_to_device - last_playback_position);
        if( wait_frames + hw_avail < minimum_buffer_free_frames )
            wait_frames += period_frames;

        return last_update_time + time_for_frames(wait_frames, current_config.format.rate);
    }

    steady_clock::time_point next_pre_underrun_wakeup(steady_clock::time_point last_update_time) const
    {
        const auto frames_to_next_underrun = written_to_device - last_playback_position;
        const auto pre = current_config.parameter.pre_underrun_trigger_frames;
        auto wait_frames = percent_frames(frames_to_next_underrun, 100 - MaximumClockDriftPercent);
        wait_frames -= std::min(pre, wait_frames);
        return last_update_time + time_for_frames(wait_frames, current_config.format.rate);
    }

    void set_timer(steady_clock::time_point t)
    {
        alpe_utils::set_timerfd_absolute(timer_pfd.descriptor().value, lbu::poll::timespec_for_duration(t.time_since_epoch()));
    }

    void clear_timer()
    {
        alpe_utils::clear_timerfd(timer_pfd.descriptor().value);
    }

    error_event set_error(error_event e)
    {
        engine_state = State::Error;
        current_buffer = {};
        clear_timer();
        return e;
    }

    error_event set_error_alsa(int err)
    {
        return set_error({error_event::Type::Alsa, err});
    }

    error_event set_error_alsa_if_not_underrun(int err)
    {
        const error_event e{error_event::Type::Alsa, err};
        return is_underrun_error(err) ? e : set_error(e);
    }

    error_event set_error_internal_and_drop()
    {
        snd_pcm_drop(pcm);
        return set_error({error_event::Type::Internal});
    }

    void clear_buffer()
    {
        last_stop_position = 0;
        last_playback_position = 0;
        written_to_device = 0;
        written_to_buffer = 0;
        buffer_at_eos = false;

        current_buffer = lbu::array_ref<char>(buffer.get(), buffer_frame_count * frame_byte_size);
    }

    void update_current_buffer()
    {
        assert(written_to_buffer >= last_playback_position);
        if( buffer_at_eos ) {
            current_buffer = {};
            return;
        }
        const auto offset = short_duration_frames(written_to_buffer % buffer_frame_count);
        const auto buffer_avail = buffer_frame_count - current_buffered_frames();
        const auto ring_avail = buffer_frame_count - offset;
        current_buffer = lbu::array_ref<char>(buffer.get() + (offset * frame_byte_size), std::min(buffer_avail, ring_avail) * frame_byte_size);
    }

    command_error update_playback_position()
    {
        const auto avail = snd_pcm_status_get_avail(pcm_status);
        const auto hw_frames = current_config.period_buffer_count * current_config.period_frames;
        if( avail > hw_frames ) {
            log_error("bug in the Alsa API detected: more available frames than buffer size (avail %li, buffer size %li)",
                      long(avail), long(hw_frames));
            return set_error_internal_and_drop();
        }
        assert(written_to_device + avail >= hw_frames);
        const auto pos = (written_to_device + avail) - hw_frames;
        assert(pos >= last_playback_position && pos <= written_to_device);
        last_playback_position = pos;
        update_current_buffer();
        return {};
    }

    command_error write_to_device(const void* src, short_duration_frames frames)
    {
        snd_pcm_sframes_t res = -1;
        if( res = snd_pcm_writei(pcm, src, frames); res < 0 )
            return set_error_alsa_if_not_underrun(res);

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
        write_test_data(src, frames);
#endif
        written_to_device += short_duration_frames(res);
        if( short_duration_frames(res) != frames )
            return UnderrunErrorEvent;
        return {};
    }

    command_error check_fill_device_fade(FadeFill fade_fill)
    {
        assert(written_to_device < fade_end_position);
        assert(written_to_buffer >= written_to_device);
        assert(written_to_device >= last_playback_position);
        const auto period_frames = current_config.period_frames;
        const auto frame_bytes = frame_byte_size;
        const auto hw_frames = current_config.period_buffer_count * period_frames;
        const auto ring_size = buffer_frame_count;
        const auto dev_written = written_to_device;
        const auto fade_end = fade_end_position;

        assert(dev_written - last_playback_position <= hw_frames);
        const auto hw_avail = hw_frames - (dev_written - last_playback_position);
        const auto write_avail = written_to_buffer - dev_written;
        const auto fade_frames = fade_end - dev_written;
        auto write_count = std::min({hw_avail, write_avail, fade_frames});

        char* const ring_base = buffer.get();
        char* const tmp_buffer = ring_base + (ring_size * frame_bytes);
        const auto param = fade_param;
        const bool is_fade_in = fade_mode == FadeMode::In;
        bool first_write = true;

        while( write_count > 0 ) {
            const auto iter_max = percent_frames(period_frames, (first_write && fade_fill == FadeFill::ShortFirst) ? FadeFillShortPercent
                                                                                                                   : 100);
            const auto iter_count = std::min(short_duration_frames(write_count), iter_max);
            const auto cur_start = written_to_device;

            alpe_utils::apply_fade(tmp_buffer, ring_base, short_duration_frames(cur_start % ring_size), ring_size,
                                   current_config.format, frame_bytes, is_fade_in, param.type, param.duration,
                                   param.duration - (fade_end - cur_start), iter_count);

            if( auto err = write_to_device(tmp_buffer, iter_count); err )
                return *err;
            write_count -= iter_count;
            first_write = false;
        }

        return {};
    }

    command_error check_fill_device(FadeFill fade_fill = FadeFill::Default)
    {
        if( written_to_device < fade_end_position ) {
            if( auto err = check_fill_device_fade(fade_fill); err )
                return *err;
        }

        assert(written_to_buffer >= written_to_device);
        assert(written_to_device >= last_playback_position);
        const auto hw_frames = current_config.period_buffer_count * current_config.period_frames;
        assert(written_to_device - last_playback_position <= hw_frames);
        const auto hw_avail = hw_frames - (written_to_device - last_playback_position);
        const auto write_avail = written_to_buffer - written_to_device;
        if( hw_avail < minimum_buffer_free_frames || write_avail == 0 || fade_mode != FadeMode::In )
            return {};

        const auto write_count = std::min(hw_avail, write_avail);
        const auto offset = written_to_device % buffer_frame_count;
        const auto ring_avail = buffer_frame_count - offset;
        const auto first_write = std::min(write_count, ring_avail);
        const auto second_write = write_count - first_write;
        const char* const ptr = buffer.get();

        if( auto err = write_to_device(ptr + (offset * frame_byte_size), first_write); err )
            return *err;
        if( second_write == 0 || hw_avail - first_write < minimum_buffer_free_frames )
            return {};
        return write_to_device(ptr, second_write);
    }

    long_duration_frames estimate_position(steady_clock::duration time_since_last_update) const
    {
        if( time_since_last_update < std::chrono::seconds(0) || time_since_last_update > MaximumPeriodTime * MaximumAlsaPeriods ) {
            log_warning("implausible %s time since last update detected",
                        time_since_last_update < std::chrono::seconds(0) ? "negative" : "large");
            return time_since_last_update < std::chrono::seconds(0) ? last_playback_position : written_to_device;
        } else {
            const auto estimated_pos = last_playback_position + frames_for_time(time_since_last_update, current_config.format.rate);
            return std::min(estimated_pos, written_to_device);
        }
    }

    long_duration_frames estimate_position() const { return estimate_position(steady_clock::now() - status_htstamp(pcm_status)); }

    command_error update_fade(FadeMode new_mode, fade_parameter new_param)
    {
        assert(written_to_buffer >= written_to_device);
        assert(written_to_device >= last_playback_position);

        if( new_mode == FadeMode::Out && new_param.duration == 0 ) { // Special case for zero length fade outs - see handle_running()
            fade_mode = new_mode;
            fade_param = new_param;
            fade_end_position = last_playback_position;
            return {};
        } else if( new_mode != FadeMode::EmergencyOut ) { // in the emergency out case we already did that shortly before, not needed again
            if( int err = snd_pcm_status(pcm, pcm_status); err < 0 )
                return set_error_alsa(err);
            if( auto s = snd_pcm_status_get_state(pcm_status); s != SND_PCM_STATE_RUNNING ) {
                if( s == SND_PCM_STATE_XRUN || s == SND_PCM_STATE_SUSPENDED )
                    return UnderrunErrorEvent;
                if( s == SND_PCM_STATE_DISCONNECTED )
                    return set_error({error_event::Type::Disconnected});
                log_error("unexpected pcm state %s", snd_pcm_state_name(s));
                return set_error_internal_and_drop();
            }
            if( auto err = update_playback_position(); err )
                return err;
        }

        snd_pcm_sframes_t rewind = snd_pcm_rewindable(pcm);
        if( rewind < 0 )
            return set_error_alsa_if_not_underrun(rewind);
        if( rewind > snd_pcm_sframes_t(written_to_device - last_playback_position) ) {
            log_warning("bad rewindable value detected - not rewinding");
            rewind = 0;
        }

        rewind = std::min(rewind, snd_pcm_sframes_t(written_to_device - estimate_position()));

        rewind -= snd_pcm_sframes_t(std::min(short_duration_frames(rewind), current_config.parameter.rewind_safeguard));
        if( new_mode == FadeMode::EmergencyOut )
            rewind = snd_pcm_sframes_t(std::min(short_duration_frames(rewind), current_config.parameter.emergency_pause_fade));

        if( rewind > 0 ) {
            const auto r = snd_pcm_rewind(pcm, snd_pcm_uframes_t(rewind));
            if( r < 0 ) {
                log_warning("rewind failed");
                return set_error_alsa_if_not_underrun(r);
            }
            const auto actual_rewind = long_duration_frames(r);
            if( actual_rewind != long_duration_frames(rewind) )
                log_warning("actual rewind different from requested: %lu != %u", static_cast<unsigned long>(actual_rewind), unsigned(rewind));
            if( actual_rewind > written_to_device - last_playback_position ) {
                log_error("actual rewind too large - beyond last playback position");
                return set_error_internal_and_drop();
            }

            written_to_device -= actual_rewind;

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
            rewind_test_data(r);
#endif
        }

        { // update fade end
            double current_fade_progress = 1.0;
            if( fade_end_position > written_to_device && fade_param.duration > 0 )
                current_fade_progress = std::max(0.0, 1.0 - double(fade_end_position - written_to_device) / fade_param.duration);
            const double new_fade_progress = alpe_utils::convert_fade_progress(current_fade_progress,
                                                                               fade_param.type, fade_mode == FadeMode::In,
                                                                               new_param.type, new_mode == FadeMode::In);
            fade_end_position = written_to_device + std::rint((1.0 - new_fade_progress) * new_param.duration);
        }

        fade_mode = new_mode;
        fade_param = new_param;

        return check_fill_device(FadeFill::ShortFirst);
    }

    event handle_stop()
    {
        if( int err = snd_pcm_prepare(pcm); err < 0 )
            return set_error_alsa(err);
        engine_state = State::Ready;

        const auto pos = written_to_device;
        const bool at_stop = (fade_mode == FadeMode::Out && pos == fade_end_position);
        const bool at_emergency_stop = (fade_mode == FadeMode::EmergencyOut && pos == fade_end_position);

        last_stop_position = pos;
        last_playback_position = pos;
        update_current_buffer();
        clear_timer();

        if( at_stream_end() || at_stop )
            return playback_stop_event{ pos };
        return playback_underrun_event{ pos, ! at_emergency_stop };
    }

    int wait_and_drop()
    {
        const auto rate = current_config.format.rate;
        // write some silence at the end
        if( auto res = writei_silence(pcm, frame_byte_size, frames_for_time(MinimumPeriodTime, rate)); res < 0 )
            return is_underrun_error(res) ? 0 : res;

        const auto wait_frames = percent_frames(written_to_device - last_playback_position, 100 + MaximumClockDriftPercent);
        set_timer(status_htstamp(pcm_status) + time_for_frames(wait_frames, rate) + MinimumWaitTime);
        lbu::poll::wait_for_event(*timer_pfd.as_pollfd());

        return snd_pcm_drop(pcm);
    }

    event handle_pre_underrun()
    {
        const bool at_stop = (fade_mode == FadeMode::Out && written_to_device == fade_end_position);

        if( ! (buffer_at_eos || at_stop) ) {
            auto err = update_fade(FadeMode::EmergencyOut,
                                   {fade_parameter::Type::Linear, current_config.parameter.emergency_pause_fade});
            if( err )
                return is_underrun_error_event(*err) ? handle_stop() : event(*err);
        }

        if( int err = wait_and_drop(); err < 0 )
            return set_error_alsa(err);

        return handle_stop();
    }

    event handle_running()
    {
        if( auto err = update_playback_position(); err )
            return event(*err);
        if( auto err = check_fill_device(); err )
            return is_underrun_error_event(*err) ? handle_stop() : event(*err);

        const auto last_update_time = status_htstamp(pcm_status);

        if( fade_mode == FadeMode::Out && fade_param.duration == 0 ) {
            // Special case for zero length fade outs: since no actual fade needs to be played back, we
            // can just use drop instead of the rewind + drain (needed for real fades) which should have
            // a significantly shorter delay. Unfortunately I don't think Alsa can report the position
            // where the playback stopped, so we need to estimate.
            if( int err = snd_pcm_drop(pcm); err < 0 )
                return set_error_alsa(err);

            if( int err = snd_pcm_status(pcm, pcm_status); err < 0 )
                return set_error_alsa(err);
            const auto pos = estimate_position(status_trigger_htstamp(pcm_status) - last_update_time);

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
            rewind_test_data(long(written_to_device - pos));
#endif

            written_to_device = pos;
            fade_end_position = pos;

            return handle_stop();
        }

        const auto pre_underrun_wakeup = next_pre_underrun_wakeup(last_update_time);
        const auto minimum_wait = steady_clock::now() + MinimumWaitTime;
        if( pre_underrun_wakeup < minimum_wait )
            return handle_pre_underrun();

        set_timer(std::max(std::min(pre_underrun_wakeup, next_hw_period_wakeup(last_update_time)), minimum_wait));
        return playback_event{ last_playback_position, last_update_time };
    }


    lbu::array_ref<const pollfd> get_poll_fds() override { return lbu::array_ref_one_element(timer_pfd.as_pollfd()); }

    bool has_buffered_events() override { return false; }

    event next_event() override
    {
        if( engine_state == State::Error )
            return log_api_error("next_event called while already in error state");

        if( int err = snd_pcm_status(pcm, pcm_status); err < 0 )
            return set_error_alsa(err);

        const auto alsa_state = snd_pcm_status_get_state(pcm_status);
        switch( alsa_state ) {
        case SND_PCM_STATE_OPEN:
        case SND_PCM_STATE_SETUP:
            if( engine_state != State::Unconfigured )
                goto state_mismatch;
            return no_event{};
        case SND_PCM_STATE_PREPARED:
            if( engine_state != State::Ready && engine_state != State::Unconfigured )
                goto state_mismatch;
            return no_event{};
        case SND_PCM_STATE_RUNNING:
            if( engine_state != State::Playing ) {
                snd_pcm_drop(pcm);
                goto state_mismatch;
            }
            return handle_running();
        case SND_PCM_STATE_XRUN:
            return handle_stop();
        case SND_PCM_STATE_SUSPENDED:
            if( engine_state == State::Playing ) {
                return handle_stop();
            } else if( engine_state == State::Ready ) {
                if( int err = snd_pcm_prepare(pcm); err < 0 )
                    return set_error_alsa(err);
            }
            return no_event{};
        case SND_PCM_STATE_DISCONNECTED:
            return set_error({error_event::Type::Disconnected});
        case SND_PCM_STATE_PAUSED: // this engine never uses the pause functionality
        case SND_PCM_STATE_DRAINING:
        default:
            log_error("unexpected pcm state %s", snd_pcm_state_name(alsa_state));
            return set_error_internal_and_drop();
        }

    state_mismatch:
        log_error("pcm state %s does not match internal state %d", snd_pcm_state_name(alsa_state), int(engine_state));
        return set_error_internal_and_drop();
    }

    configuration_result configure(lbu::alsa::pcm_format requested_format, std::chrono::milliseconds requested_period_duration) override
    {
        if( engine_state != State::Unconfigured && engine_state != State::Ready )
            return log_api_error("configure called while not ready");
        if( ! lbu::alsa::pcm_format::is_signed_linear_pcm_format_type(requested_format.type) )
            return log_api_error("only signed linear pcm formats supported");
        if( requested_format.channels == 0 )
            return log_api_error("zero channels requested");
        if( requested_format.rate > MaximumSampleRate )
            return log_api_error("unsupported large sample rate %u", requested_format.rate);
        if( requested_period_duration < MinimumPeriodTime || requested_period_duration > MaximumPeriodTime )
            return log_api_error("requested_period_duration %ld out of range", long(requested_period_duration.count()));

        log_info("configure requested format %s, %u channels, %u Hz, %u ms period duration",
                 snd_pcm_format_name(requested_format.type), requested_format.channels, requested_format.rate,
                 unsigned(requested_period_duration.count()));

        int err;
        configuration cfg;

        {
            lbu::alsa::hardware_params hw_params;
            if( err = snd_pcm_hw_free(pcm); err < 0 )
                return set_error_alsa(err);
            if( err = snd_pcm_hw_params_any(pcm, hw_params); err < 0 )
                return set_error_alsa(err);
            if( err = snd_pcm_hw_params_set_access(pcm, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED); err < 0 )
                return set_error_alsa(err);
            if( err = snd_pcm_hw_params_set_channels(pcm, hw_params, requested_format.channels); err < 0 )
                return set_error_alsa(err);
            cfg.format.channels = requested_format.channels;
            cfg.format.rate = lbu::alsa::hardware_params::set_best_matching_sample_rate(pcm, hw_params, requested_format.rate, &err);
            if( err < 0 )
                return set_error_alsa(err);
            if( cfg.format.rate > MaximumSampleRate ) {
                log_error("unexpected large hardware sample rate %u", cfg.format.rate);
                return set_error({error_event::Type::Internal});
            }
            cfg.format.type = lbu::alsa::hardware_params::set_best_signed_linear_pcm_format_type(pcm, hw_params, requested_format.type, &err);
            if( err < 0 )
                return set_error_alsa(err);

            const auto requested_period_size = frames_for_time(requested_period_duration, cfg.format.rate);
            cfg.period_frames = requested_period_size;
            if( err = snd_pcm_hw_params_set_period_size(pcm, hw_params, cfg.period_frames, 0); err < 0 ) {
                snd_pcm_uframes_t period_min = 0;
                snd_pcm_uframes_t period_max = 0;
                const auto minimum_period_size = frames_for_time(MinimumPeriodTime, cfg.format.rate);
                if( err = snd_pcm_hw_params_get_period_size_min(hw_params, &period_min, nullptr); err < 0 )
                    return set_error_alsa(err);
                if( err = snd_pcm_hw_params_get_period_size_max(hw_params, &period_max, nullptr); err < 0 )
                    return set_error_alsa(err);
                log_info("requested period size %u not available, range is %lu - %lu frames",
                         requested_period_size, period_min, period_max);
                int dir = 0;
                if( cfg.period_frames < period_min )
                    cfg.period_frames = period_min;
                else if( cfg.period_frames > period_max )
                    cfg.period_frames = std::max<short_duration_frames>(period_max, minimum_period_size);
                else
                    dir = 1;
                if( err = snd_pcm_hw_params_set_period_size(pcm, hw_params, cfg.period_frames, dir); err < 0 ) {
                    snd_pcm_uframes_t period_tmp = cfg.period_frames;
                    if( err = snd_pcm_hw_params_set_period_size_near(pcm, hw_params, &period_tmp, &dir); err < 0 )
                        return set_error_alsa(err);
                }
                snd_pcm_uframes_t period_tmp = 0;
                if( err = snd_pcm_hw_params_get_period_size(hw_params, &period_tmp, nullptr); err < 0 )
                    return set_error_alsa(err);
                if( period_tmp < minimum_period_size ) {
                    return set_error({error_event::Type::UnsupportedLowLatencyHardware});
                } else if( period_tmp > MaximumPeriodFrames ) {
                    log_error("unexpected large required hardware period size %lu", period_tmp);
                    return set_error({error_event::Type::Internal});
                }
                cfg.period_frames = period_tmp;
            }

            cfg.period_buffer_count = std::clamp<unsigned>(lbu::idiv_ceil(2 * requested_period_size, cfg.period_frames),
                                                           2, MaximumAlsaPeriods);
            if( err = snd_pcm_hw_params_set_periods(pcm, hw_params, cfg.period_buffer_count, 0); err < 0 ) {
                unsigned periods_min = 0;
                unsigned periods_max = 0;
                if( err = snd_pcm_hw_params_get_periods_min(hw_params, &periods_min, nullptr); err < 0 )
                    return set_error_alsa(err);
                if( err = snd_pcm_hw_params_get_periods_max(hw_params, &periods_max, nullptr); err < 0 )
                    return set_error_alsa(err);
                log_info("requested period count %u not available, range is %u - %u", cfg.period_buffer_count, periods_min, periods_max);
                int dir = 0;
                unsigned tmp = cfg.period_buffer_count;
                if( tmp < periods_min )
                    tmp = periods_min;
                else if( tmp > periods_max )
                    tmp = std::max<unsigned>(2, periods_max);
                else
                    dir = 1;
                if( err = snd_pcm_hw_params_set_periods(pcm, hw_params, tmp, dir); err < 0 ) {
                    if( err = snd_pcm_hw_params_set_periods_near(pcm, hw_params, &tmp, &dir); err < 0 )
                        return set_error_alsa(err);
                }
                if( err = snd_pcm_hw_params_get_periods(hw_params, &tmp, nullptr); err < 0 )
                    return set_error_alsa(err);
                if( tmp < 2 || tmp > MaximumAlsaPeriods ) {
                    log_error("unexpected required period buffer count %u", tmp);
                    return set_error({error_event::Type::Internal});
                }
                cfg.period_buffer_count = tmp;
            }

            if( err = snd_pcm_hw_params(pcm, hw_params); err < 0 )
                return set_error_alsa(err);
        }

        log_info("configure actual format %s, %u Hz, %u frames period duration, %u periods",
                 snd_pcm_format_name(cfg.format.type), cfg.format.rate, cfg.period_frames, cfg.period_buffer_count);

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
        fseek(test_out, 0, SEEK_SET);
        log_warning("writing test file %s", TestFile);
#endif

        {
            lbu::alsa::software_params sw_params;
            if( err = snd_pcm_sw_params_current(pcm, sw_params); err < 0 )
                return set_error_alsa(err);

            if( err = snd_pcm_sw_params_set_start_threshold(pcm, sw_params, std::numeric_limits<long>::max()); err < 0 )
                return set_error_alsa(err);

            if( err = snd_pcm_sw_params_set_tstamp_mode(pcm, sw_params, SND_PCM_TSTAMP_ENABLE); err < 0 )
                return set_error_alsa(err);
            if( err = snd_pcm_sw_params_set_tstamp_type(pcm, sw_params, SND_PCM_TSTAMP_TYPE_MONOTONIC); err < 0 )
                return set_error_alsa(err);

            if( err = snd_pcm_sw_params(pcm, sw_params); err < 0 )
                return set_error_alsa(err);
        }

        current_config = cfg;
        frame_byte_size = lbu::alsa::pcm_format::frame_packed_byte_size(cfg.format.type, cfg.format.channels);
        minimum_buffer_free_frames = percent_frames(cfg.period_frames * cfg.period_buffer_count, MinimumBufferFreePercent);
        engine_state = State::Ready;
        return update_parameters(default_engine_parameter(cfg.format.rate));
    }

    configuration_result update_parameters(engine_parameter param) override
    {
        if( engine_state != State::Ready )
            return log_api_error("update_parameters called while not in ready state");

        current_config.parameter = param;

        if( alpe_utils::required_buffer_too_large(current_config, frame_byte_size) ) {
            log_error("required buffer too large for given configuration and parameter");
            return set_error({error_event::Type::Internal});
        }

        buffer_frame_count = current_config.buffer_count() * current_config.period_frames;
        buffer.reset(lbu::xmalloc_bytes<char>((buffer_frame_count + current_config.period_frames) * frame_byte_size));
        clear_buffer();

        log_info("updated parameter to %u additional buffer, %u pre underrun trigger frames, %u rewind safeguard, %u emergency pause fade",
                 param.additional_buffer_count, param.pre_underrun_trigger_frames, param.rewind_safeguard, param.emergency_pause_fade);

        return current_config;
    }

    command_error fade_in(fade_parameter param) override
    {
        if( engine_state == State::Ready ) {
            fade_mode = FadeMode::In;
            fade_param = param;
            fade_end_position = last_playback_position + param.duration;

            if( auto err = check_fill_device(); err ) {
                if( is_underrun_error_event(*err) ) {
                    log_error("unexpected underrun before start");
                    return set_error_internal_and_drop();
                }
                return err;
            }
            if( written_to_device == last_playback_position )
                return log_api_error("cannot start without data");

            if( int err = snd_pcm_start(pcm); err < 0 )
                return set_error_alsa(err);
            engine_state = State::Playing;
            set_timer(steady_clock::now());
            return {};
        }

        if( engine_state != State::Playing )
            return log_api_error("fade_in called while not ready");

        set_timer(steady_clock::now());
        if( auto err = update_fade(FadeMode::In, param); err ) {
            if( ! is_underrun_error_event(*err) )
                return err;
            // underrun error, will be reported in next_event
        }
        return {};
    }

    command_error fade_out(fade_parameter param) override
    {
        if( engine_state != State::Playing )
            return log_api_error("fade_out called while not playing");

        set_timer(steady_clock::now());
        if( auto err = update_fade(FadeMode::Out, param); err ) {
            if( ! is_underrun_error_event(*err) )
                return err;
            // underrun error, will be reported in next_event
        }
        return {};
    }

    command_error advance_buffer(short_duration_frames frames, EndOfStream eos) override
    {
        if( current_buffer.byte_size() < frames * frame_byte_size )
            return log_api_error("cannot advance buffer beyond its size");

        written_to_buffer += frames;
        buffer_at_eos = (eos == EndOfStream::Yes);
        update_current_buffer();

        if( engine_state == State::Playing ) {
            if( auto err = check_fill_device(); err ) {
                if( ! is_underrun_error_event(*err) )
                    return err;
                // underrun error, don't report here but just make client call next_event
                set_timer(steady_clock::now());
                return {};
            }
            if( current_config.buffer_count() == 2 ) {
                // Update the timer here only when having two buffers, because otherwise it is unlikely
                // that the next wakeup point changed. This is an optimization to reduce wakeups anyway,
                // not needed for correct operation.
                const auto last_update_time = status_htstamp(pcm_status);
                set_timer(std::min(next_pre_underrun_wakeup(last_update_time), next_hw_period_wakeup(last_update_time)));
            }
        }
        return {};
    }

    command_error drop_buffers() override
    {
        if( engine_state == State::Playing ) {
            if( int err = snd_pcm_drop(pcm); err < 0 )
                return set_error_alsa(err);
            if( int err = snd_pcm_prepare(pcm); err < 0 )
                return set_error_alsa(err);
            clear_timer();
            engine_state = State::Ready;
        }

        if( engine_state != State::Ready )
            return log_api_error("drop_buffers called while not ready");

        clear_buffer();
        return {};
    }

    ~alpe_alsa() override
    {
        if( engine_state == State::Playing )
            snd_pcm_drop(pcm);

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
        fclose(test_out);
#endif
    }

    alpe_alsa(snd_pcm_t* pcm_device) : pcm(pcm_device)
    {
        timer_pfd.reset(lbu::fd(timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK|TFD_CLOEXEC)), lbu::poll::FlagsReadReady);
        if( ! timer_pfd.descriptor() )
            lbu::unexpected_system_error(errno);

#ifdef ALPE_DEBUG_WRITE_OUTPUT_TO_TESTFILE
        test_out = fopen(TestFile, "wb");
#endif
    }
};

std::unique_ptr<alpe> alpe::create_alsa(snd_pcm_t* pcm_device) { return std::unique_ptr<alpe>(new alpe_alsa(pcm_device)); }
