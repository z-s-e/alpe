/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "alpe.h"

#include <cstdarg>
#include <stdio.h>


static const char* log_level_string(alpe::LogLevel level)
{
    switch( level ) {
    case alpe::LogLevel::Info:      return "INFO";
    case alpe::LogLevel::Warning:   return "WARN";
    case alpe::LogLevel::Error:     return "ERR ";
    }
    return "????";
}

void alpe::debug_logger::log(alpe::LogLevel level, const char* message)
{
#ifdef NDEBUG
    if( level == alpe::LogLevel::Info )
        return;
#endif
    char clock[13] = {};
    print_unix_time_day_clock(lbu::array_ref<char>(clock));
    fprintf(stderr, "%s libalpe %s: %s\n", clock, log_level_string(level), message);
}

lbu::array_ref<char> alpe::debug_logger::print_unix_time_day_clock(lbu::array_ref<char> dst)
{
    const auto t = std::chrono::floor<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    const int ms = t % 1000;
    const time_t secs = t / 1000;
    struct tm localtime;
    localtime_r(&secs, &localtime);
    snprintf(dst.data(), dst.size(), "%2d:%02d:%02d.%03d", localtime.tm_hour, localtime.tm_min, localtime.tm_sec, ms);
    return dst.sub_last(12);
}

alpe::debug_logger alpe::stderr_logger;

namespace {
struct message_printer {
    char buffer[256] = {};
    message_printer(const char* fmt, va_list vlist) { vsnprintf(buffer, sizeof(buffer), fmt, vlist); }
};
}

void alpe::log(alpe::debug_logger* l, alpe::LogLevel level, const char* fmt, ...)
{
    if( l == nullptr )
        return;

    va_list ap;
    va_start(ap, fmt);
    message_printer p(fmt, ap);
    va_end(ap);
    l->log(level, p.buffer);
}

void alpe::log_info(const char* fmt, ...) const
{
    if( logger == nullptr )
        return;

    va_list ap;
    va_start(ap, fmt);
    message_printer p(fmt, ap);
    va_end(ap);
    logger->log(LogLevel::Info, p.buffer);
}

void alpe::log_warning(const char* fmt, ...) const
{
    if( logger == nullptr )
        return;

    va_list ap;
    va_start(ap, fmt);
    message_printer p(fmt, ap);
    va_end(ap);
    logger->log(LogLevel::Warning, p.buffer);
}

void alpe::log_error(const char* fmt, ...) const
{
    if( logger == nullptr )
        return;

    va_list ap;
    va_start(ap, fmt);
    message_printer p(fmt, ap);
    va_end(ap);
    logger->log(LogLevel::Error, p.buffer);
}

alpe::error_event alpe::log_api_error(const char* fmt, ...) const
{
    if( logger == nullptr )
        return error_event{error_event::Type::Api};

    va_list ap;
    va_start(ap, fmt);
    message_printer p(fmt, ap);
    va_end(ap);
    logger->log(LogLevel::Error, p.buffer);
    return error_event{error_event::Type::Api};
}
