/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "alpe_utils.h"

#include <lbu/fd_stream.h>
#include <type_traits>
#include <vector>


enum class StreamCommand {
    Invalid,
    Quit,
    Hello,
    HelloExtended,
    Configure,
    UpdateParameters,
    FadeIn,
    FadeInPlaying,
    FadeOut,
    AddBuffer,
    DropBuffers,
    Last = DropBuffers
};

enum class StreamEvent {
    Error,
    ConfigureResult,
    Playback,
    Stop,
    Underrun,
    Last = Underrun
};

#if __BYTE_ORDER != __LITTLE_ENDIAN
#error Only little endian systems supported
#endif

static const char HelloMessage[] = "alpe-0?\n";
static const char HelloAnswer[] = "alpe-0!\n";

constexpr size_t HelloSize = sizeof(HelloMessage); // for now including the zero-termination, see below

// Command/Stream packets:
// - hello/answer: HelloMessage or HelloAnswer (without zero-termination)
//          + 1 byte "additional data size" (for future extension) + [0-255] bytes additional data
//          (for now sneakily reuse zero-termination of the string as the no-additional-data size)
// - all commands: 1 byte StreamCommand at start
// - all events: 1 byte StreamEvent at start

// The alpe_stream implementation uses blocking IO for simplicity, since it does not control the audio hardware
// directly, and thus does not need to react quickly to asynchronous events such as imminent underrun.
// The proxy_run however assumes the instance requires a timely processing of its events, and so only uses
// non-blocking IO.

// Regarding clocks: To avoid complicated clock synchronization, a primitive scheme is used; the proxy sends the time duration
// from the last timestamp to just before sending the playback event. The receiver converts that into a local clock value,
// disregarding the time for the transfer of the event. While this is obviously not very precise, it should be good enough for
// the typical player time position display in seconds and does not drift.


template< typename T >
static bool blocking_read(lbu::stream::abstract_input_stream* in, T* buf)
{
    static_assert( ! std::is_pointer_v<T> && std::is_trivial_v<T>);
    return in->read(buf, sizeof(T), lbu::stream::Mode::Blocking) >= 0;
}

template< typename T >
static bool blocking_write(lbu::stream::abstract_output_stream* out, const T* buf)
{
    static_assert( ! std::is_pointer_v<T> && std::is_trivial_v<T>);
    return out->write(buf, sizeof(T), lbu::stream::Mode::Blocking) >= 0;
}

static bool blocking_write_byte(lbu::stream::abstract_output_stream* out, uint8_t c)
{
    return blocking_write(out, &c);
}

static bool blocking_write_flush_byte(lbu::stream::abstract_output_stream* out, uint8_t c)
{
    return blocking_write_byte(out, c) && out->flush_buffer();
}

template< typename T >
static bool nonblocking_write(lbu::stream::abstract_output_stream* out, const T* buf)
{
    static_assert( ! std::is_pointer_v<T> && std::is_trivial_v<T>);
    return out->write(buf, sizeof(T), lbu::stream::Mode::NonBlocking) == sizeof(T);
}

static bool nonblocking_write_byte(lbu::stream::abstract_output_stream* out, uint8_t c)
{
    return nonblocking_write(out, &c);
}

static bool is_digit(char c)
{
    return c >= 0x30 && c <= 0x39;
}

static bool write_hello(lbu::stream::abstract_output_stream* out)
{
    if( out->write(HelloMessage, HelloSize, lbu::stream::Mode::Blocking) < 0 )
        return false;
    return out->flush_buffer();
}

static bool write_hello_answer(lbu::stream::abstract_output_stream* out)
{
    if( out->write(HelloAnswer, HelloSize, lbu::stream::Mode::NonBlocking) != HelloSize )
        return false;
    return out->flush_buffer(lbu::stream::Mode::NonBlocking);
}

static bool read_hello_answer(lbu::stream::abstract_input_stream* in)
{
    char buf[HelloSize];
    if( in->read(buf, HelloSize, lbu::stream::Mode::Blocking) < 0 )
        return false;
    if( std::memcmp(buf, HelloAnswer, HelloSize) != 0 )
        return false;
    return true;
}

constexpr size_t PcmFormatSize = 4 + 2 + 4;

static bool write_pcm_format(lbu::stream::abstract_output_stream* out, lbu::alsa::pcm_format f)
{
    int32_t tmp = int32_t(f.type);
    return blocking_write(out, &tmp) && blocking_write(out, &f.channels) && blocking_write(out, &f.rate);
}

static bool nonblocking_write_pcm_format(lbu::stream::abstract_output_stream* out, lbu::alsa::pcm_format f)
{
    int32_t tmp = int32_t(f.type);
    return nonblocking_write(out, &tmp) && nonblocking_write(out, &f.channels) && nonblocking_write(out, &f.rate);
}

static bool read_pcm_format(lbu::stream::abstract_input_stream* in, lbu::alsa::pcm_format* f)
{
    int32_t tmp;
    if( ! blocking_read(in, &tmp) )
        return false;
    f->type = snd_pcm_format_t(tmp);
    return blocking_read(in, &f->channels) && blocking_read(in, &f->rate);
}

constexpr size_t FadeParameterSize = 4 + 4;

static bool write_fade_parameter(lbu::stream::abstract_output_stream* out, alpe::fade_parameter p)
{
    int32_t tmp = int32_t(p.type);
    return blocking_write(out, &tmp) && blocking_write(out, &p.duration);
}

static bool write_error_event(lbu::stream::abstract_output_stream* out, alpe::error_event e)
{
    const int32_t type = int32_t(e.type);
    const int32_t alsa_errno = e.alsa_errno;
    const bool need_reset = true; // obsolete member, kept for protocol compat
    return nonblocking_write_byte(out, uint8_t(StreamEvent::Error))
        && nonblocking_write(out, &type)
        && nonblocking_write(out, &need_reset)
        && nonblocking_write(out, &alsa_errno)
        && out->flush_buffer(lbu::stream::Mode::NonBlocking);
}

static std::optional<alpe::error_event> read_error_event(lbu::stream::abstract_input_stream* in)
{
    alpe::error_event e;
    int32_t tmp;
    bool need_reset; // obsolete member, kept for protocol compat
    if( ! blocking_read(in, &tmp) )
        return {};
    e.type = static_cast<alpe::error_event::Type>(tmp);
    if( ! blocking_read(in, &need_reset) )
        return {};
    if( ! blocking_read(in, &tmp) )
        return {};
    e.alsa_errno = tmp;
    return e;
}

static bool write_configuration_event(lbu::stream::abstract_output_stream* out, alpe::configuration cfg)
{
    return nonblocking_write_byte(out, uint8_t(StreamEvent::ConfigureResult))
           && nonblocking_write_pcm_format(out, cfg.format)
           && nonblocking_write(out, &cfg.period_frames)
           && nonblocking_write(out, &cfg.period_buffer_count)
           && nonblocking_write(out, &cfg.parameter.additional_buffer_count)
           && nonblocking_write(out, &cfg.parameter.pre_underrun_trigger_frames)
           && nonblocking_write(out, &cfg.parameter.rewind_safeguard)
           && nonblocking_write(out, &cfg.parameter.emergency_pause_fade)
           && out->flush_buffer(lbu::stream::Mode::NonBlocking);
}

static std::optional<alpe::configuration> read_configuration_event(lbu::stream::abstract_input_stream* in)
{
    alpe::configuration cfg;
    if( read_pcm_format(in, &cfg.format)
        && blocking_read(in, &cfg.period_frames)
        && blocking_read(in, &cfg.period_buffer_count)
        && blocking_read(in, &cfg.parameter.additional_buffer_count)
        && blocking_read(in, &cfg.parameter.pre_underrun_trigger_frames)
        && blocking_read(in, &cfg.parameter.rewind_safeguard)
        && blocking_read(in, &cfg.parameter.emergency_pause_fade) )
        return cfg;
    return {};
}

static bool write_playback_event(lbu::stream::abstract_output_stream* out, alpe::playback_event e)
{
    const uint64_t pos = e.position;
    const auto now = std::chrono::steady_clock::now();
    const uint64_t t = uint64_t(std::chrono::nanoseconds(now - e.time).count());
    return nonblocking_write_byte(out, uint8_t(StreamEvent::Playback))
        && nonblocking_write(out, &pos)
        && nonblocking_write(out, &t)
        && out->flush_buffer(lbu::stream::Mode::NonBlocking);
}

static std::optional<alpe::playback_event> read_playback_event(lbu::stream::abstract_input_stream* in)
{
    alpe::playback_event e;
    const auto now = std::chrono::steady_clock::now();
    if( ! blocking_read(in, &e.position) )
        return {};
    uint64_t tmp;
    if( ! blocking_read(in, &tmp) )
        return {};
    e.time = now - std::chrono::nanoseconds(tmp);
    return e;
}

static bool write_playback_stop_event(lbu::stream::abstract_output_stream* out, alpe::playback_stop_event e)
{
    const uint64_t pos = e.position;
    return nonblocking_write_byte(out, uint8_t(StreamEvent::Stop))
           && nonblocking_write(out, &pos)
           && out->flush_buffer(lbu::stream::Mode::NonBlocking);
}

static std::optional<alpe::playback_stop_event> read_playback_stop_event(lbu::stream::abstract_input_stream* in)
{
    alpe::playback_stop_event e;
    if( blocking_read(in, &e.position) )
        return e;
    return {};
}

static bool write_playback_underrun_event(lbu::stream::abstract_output_stream* out, alpe::playback_underrun_event e)
{
    const uint64_t pos = e.position;
    return nonblocking_write_byte(out, uint8_t(StreamEvent::Underrun))
           && nonblocking_write(out, &pos)
           && nonblocking_write(out, &e.hard_underrun)
           && out->flush_buffer(lbu::stream::Mode::NonBlocking);
}

static std::optional<alpe::playback_underrun_event> read_playback_underrun_event(lbu::stream::abstract_input_stream* in)
{
    alpe::playback_underrun_event e;
    if( blocking_read(in, &e.position) && blocking_read(in, &e.hard_underrun) )
        return e;
    return {};
}


struct alpe_stream : public alpe {
    lbu::stream::abstract_input_stream* in = {};
    lbu::stream::abstract_output_stream* out = {};
    pollfd pfds[2];
    lbu::unique_cpod<char> period_buffer;
    size_t frame_byte_size = 0;
    short_duration_frames buffer_frame_count = 0;

    lbu::unique_fd timeout_timerfd;
    std::chrono::steady_clock::time_point next_timeout;


    void update_timeout()
    {
        next_timeout = std::chrono::steady_clock::now() + time_for_frames(buffer_frame_count, current_config.format.rate);
        alpe_utils::set_timerfd_absolute(timeout_timerfd.get().value, lbu::poll::timespec_for_duration(next_timeout.time_since_epoch()));
    }

    void clear_timeout()
    {
        alpe_utils::clear_timerfd(timeout_timerfd.get().value);
    }

    void set_playing()
    {
        engine_state = State::Playing;
        update_timeout();
    }

    void set_ready()
    {
        engine_state = State::Ready;
        clear_timeout();
    }

    error_event set_error(error_event e)
    {
        engine_state = State::Error;
        current_buffer = {};
        return e;
    }

    error_event set_error_alsa(int err)
    {
        return set_error({error_event::Type::Alsa, err});
    }

    error_event set_error_disconnected()
    {
        return set_error({error_event::Type::Disconnected});
    }

    void update_current_buffer()
    {
        if( buffer_at_eos ) {
            current_buffer = {};
            return;
        }
        assert(written_to_buffer >= last_playback_position);
        assert(written_to_buffer - last_playback_position <= buffer_frame_count);
        const auto avail = std::min<short_duration_frames>(buffer_frame_count - (written_to_buffer - last_playback_position),
                                                           current_config.period_frames);
        current_buffer = lbu::array_ref<char>(period_buffer.get(), size_t(avail) * frame_byte_size);
    }

    void clear_buffer()
    {
        last_playback_position = 0;
        written_to_buffer = 0;
        buffer_at_eos = false;
        update_current_buffer();
    }

    void update_playback_position(long_duration_frames new_position)
    {
        last_playback_position = new_position;
        update_current_buffer();
    }

    void handle_stop(long_duration_frames stop_position)
    {
        update_playback_position(stop_position);
        set_ready();
    }

    configuration_result read_configuration_result()
    {
        uint8_t event_byte;
        if( ! blocking_read(in, &event_byte) )
            return set_error_disconnected();
        switch( static_cast<StreamEvent>(event_byte) ) {
        case StreamEvent::Error:
            if( auto e = read_error_event(in); e )
                return set_error(*e);
            return set_error_disconnected();
        case StreamEvent::ConfigureResult:
            if( auto cfg = read_configuration_event(in); cfg ) {
                current_config = *cfg;
                engine_state = State::Ready;
                frame_byte_size = lbu::alsa::pcm_format::frame_packed_byte_size(current_config.format.type, current_config.format.channels);
                buffer_frame_count = current_config.buffer_count() * current_config.period_frames;
                period_buffer.reset(lbu::xmalloc_bytes<char>(current_config.period_frames * frame_byte_size));
                clear_buffer();
                return current_config;
            }
            return set_error_disconnected();
        default:
            log_error("unexpected configuration result response %u", event_byte);
            return set_error({error_event::Type::Internal});
        }
    }

    command_error do_handshake()
    {
        log_info("start handshake");
        if( ! write_hello(out) )
            return set_error_disconnected();
        if( ! read_hello_answer(in) )
            return in->has_error() ? set_error_disconnected() : set_error({error_event::Type::Protocol});
        return {};
    }


    lbu::array_ref<const pollfd> get_poll_fds() override { return lbu::array_ref<const pollfd>(pfds); }

    bool has_buffered_events() override
    {
        if( engine_state == State::Error )
            return false;
        return bool(in->get_buffer(lbu::stream::Mode::NonBlocking)) || in->has_error() || in->at_end();
    }

    event next_event() override
    {
        if( engine_state == State::Error )
            return log_api_error("next_event called while already in error state");

        if( engine_state == State::Playing && std::chrono::steady_clock::now() > next_timeout )
            return set_error_disconnected();

        uint8_t event_byte;
        if( auto r = in->read(&event_byte, 1, lbu::stream::Mode::NonBlocking); r <= 0 ) {
            if( in->has_error() || in->at_end() )
                return set_error_disconnected();
            return no_event{};
        }

        if( engine_state == State::Unconfigured || event_byte > unsigned(StreamEvent::Last) ) {
            log_error("unexpected next_event event %u", event_byte);
            return set_error({error_event::Type::Internal});
        }

        switch( static_cast<StreamEvent>(event_byte) ) {
        case StreamEvent::Error:
            if( auto e = read_error_event(in); e )
                return set_error(*e);
            return set_error_disconnected();
        case StreamEvent::Playback:
            if( engine_state != State::Playing ) {
                log_error("playback event while not playing");
                return set_error({error_event::Type::Internal});
            }
            if( auto e = read_playback_event(in); e ) {
                update_playback_position(e->position);
                update_timeout();
                return *e;
            }
            return set_error_disconnected();
        case StreamEvent::Stop:
            if( engine_state != State::Playing ) {
                log_error("playback stop event while not playing");
                return set_error({error_event::Type::Internal});
            }
            if( auto e = read_playback_stop_event(in); e ) {
                handle_stop(e->position);
                return *e;
            }
            return set_error_disconnected();
        case StreamEvent::Underrun:
            if( engine_state != State::Playing ) {
                log_error("playback underrun event while not playing");
                return set_error({error_event::Type::Internal});
            }
            if( auto e = read_playback_underrun_event(in); e ) {
                handle_stop(e->position);
                return *e;
            }
            return set_error_disconnected();
        default:
            log_error("unexpected event %u", event_byte);
            return set_error({error_event::Type::Internal});
        }
    }

    configuration_result configure(lbu::alsa::pcm_format requested_format, std::chrono::milliseconds requested_period_duration) override
    {
        if( engine_state != State::Unconfigured && engine_state != State::Ready )
            return log_api_error("configure called while not ready");

        if( engine_state == State::Unconfigured ) {
            if( auto e = do_handshake(); e )
                return *e;
        }

        log_info("configure requested format %s, %u channels, %u Hz, %u ms period duration",
                 snd_pcm_format_name(requested_format.type), requested_format.channels, requested_format.rate,
                 unsigned(requested_period_duration.count()));

        uint64_t tmp = uint64_t(requested_period_duration.count());
        if( blocking_write_byte(out, uint8_t(StreamCommand::Configure))
            && write_pcm_format(out, requested_format)
            && blocking_write(out, &tmp)
            && out->flush_buffer() ) {
            const auto cfg = read_configuration_result();
            if( std::holds_alternative<alpe::configuration>(cfg) ) {
                const auto c = std::get<alpe::configuration>(cfg);
                log_info("configure actual format %s, %u Hz, %u frames period duration, %u periods, %u additional buffer, "
                         "%u pre underrun trigger frames, %u rewind safeguard, %u emergency pause fade",
                         snd_pcm_format_name(c.format.type), c.format.rate, c.period_frames, c.period_buffer_count,
                         c.parameter.additional_buffer_count, c.parameter.pre_underrun_trigger_frames,
                         c.parameter.rewind_safeguard, c.parameter.emergency_pause_fade);
            }
            return cfg;
        }
        return set_error_disconnected();
    }

    configuration_result update_parameters(engine_parameter param) override
    {
        if( engine_state != State::Ready )
            return log_api_error("update_parameters called while not in ready state");

        if( blocking_write_byte(out, uint8_t(StreamCommand::UpdateParameters))
            && blocking_write(out, &param.additional_buffer_count)
            && blocking_write(out, &param.pre_underrun_trigger_frames)
            && blocking_write(out, &param.rewind_safeguard)
            && blocking_write(out, &param.emergency_pause_fade)
            && out->flush_buffer() ) {
            const auto cfg = read_configuration_result();
            if( std::holds_alternative<alpe::configuration>(cfg) ) {
                const auto c = std::get<alpe::configuration>(cfg);
                log_info("updated parameter to %u additional buffer, %u pre underrun trigger frames, %u rewind safeguard, %u emergency pause fade",
                         c.parameter.additional_buffer_count, c.parameter.pre_underrun_trigger_frames,
                         c.parameter.rewind_safeguard, c.parameter.emergency_pause_fade);
            }
            return cfg;
        }

        return set_error_disconnected();
    }

    command_error fade_in(fade_parameter param) override
    {
        if( engine_state != State::Playing && engine_state != State::Ready )
            return log_api_error("fade_in called while not ready");

        if( blocking_write_byte(out, uint8_t(engine_state == State::Playing ? StreamCommand::FadeInPlaying
                                                                            : StreamCommand::FadeIn))
            && write_fade_parameter(out, param)
            && out->flush_buffer() ) {
            set_playing();
            return {};
        }
        return set_error_disconnected();
    }

    command_error fade_out(fade_parameter param) override
    {
        if( engine_state != State::Playing )
            return log_api_error("fade_out called while not playing");

        if( blocking_write_byte(out, uint8_t(StreamCommand::FadeOut))
            && write_fade_parameter(out, param)
            && out->flush_buffer() )
            return {};
        return set_error_disconnected();
    }

    command_error advance_buffer(short_duration_frames frames, EndOfStream eos) override
    {
        if( current_buffer.byte_size() < size_t(frames) * frame_byte_size )
            return log_api_error("cannot advance buffer beyond its size");

        buffer_at_eos = (eos == EndOfStream::Yes);
        written_to_buffer += frames;
        update_current_buffer();

        if( blocking_write_byte(out, uint8_t(StreamCommand::AddBuffer))
            && blocking_write(out, &buffer_at_eos)
            && blocking_write(out, &frames)
            && out->write(period_buffer.get(), size_t(frames) * frame_byte_size, lbu::stream::Mode::Blocking) >= 0
            && out->flush_buffer() )
            return {};
        return set_error_disconnected();
    }

    command_error drop_buffers() override
    {
        if( engine_state != State::Playing && engine_state != State::Ready )
            return log_api_error("drop_buffers called while not ready");

        if( ! blocking_write_flush_byte(out, uint8_t(StreamCommand::DropBuffers)) )
            return set_error_disconnected();

        clear_buffer();

        while( engine_state == State::Playing ) {
            uint8_t event_byte;
            if( ! blocking_read(in, &event_byte) )
                return set_error_disconnected();
            switch( static_cast<StreamEvent>(event_byte) ) {
            case StreamEvent::Error:
                if( auto e = read_error_event(in); e )
                    return set_error(*e);
                return set_error_disconnected();
            case StreamEvent::Playback:
                if( ! read_playback_event(in) )
                    return set_error_disconnected();
                break;
            case StreamEvent::Stop:
                if( ! read_playback_stop_event(in) )
                    return set_error_disconnected();
                set_ready();
                break;
            case StreamEvent::Underrun:
                if( ! read_playback_underrun_event(in) )
                    return set_error_disconnected();
                set_ready();
                break;
            default:
                log_error("unexpected event %u while waiting for drop", event_byte);
                return set_error({error_event::Type::Internal});
            }
        }

        return {};
    }

    ~alpe_stream() override
    {
        if( engine_state != State::Error && engine_state != State::Unconfigured )
            blocking_write_flush_byte(out, uint8_t(StreamCommand::Quit));
    }

    alpe_stream(lbu::stream::abstract_input_stream* in_stream, pollfd pfd, lbu::stream::abstract_output_stream* out_stream)
        : in(in_stream), out(out_stream)
    {
        timeout_timerfd.reset(lbu::fd(timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK|TFD_CLOEXEC)));
        if( ! timeout_timerfd )
            lbu::unexpected_system_error(errno);

        pfds[0] = pfd;
        pfds[1] = lbu::poll::poll_fd(*timeout_timerfd, lbu::poll::FlagsReadReady);
    }
};

std::unique_ptr<alpe> alpe::create_stream(lbu::stream::abstract_input_stream* in_buffered_stream,
                                          pollfd in_stream_pollfd,
                                          lbu::stream::abstract_output_stream* out_buffered_stream)
{
    assert(in_buffered_stream->manages_buffer() && out_buffered_stream->manages_buffer());
    return std::unique_ptr<alpe>(new alpe_stream(in_buffered_stream, in_stream_pollfd, out_buffered_stream));
}


struct alpe_proxy {
    lbu::stream::abstract_input_stream* const in = {};
    lbu::stream::abstract_output_stream* const out = {};
    alpe* const instance = {};
    alpe::engine_parameter_system_preset* const preset = {};

    size_t frame_byte_size = 0;

    lbu::stream::incremental_reader reader;
    StreamCommand next_command = StreamCommand::Hello;
    char command_buffer[255];

    alpe::short_duration_frames add_buffer_frames = 0;
    bool add_buffer_eos = false;


    bool log_would_block(bool send_result)
    {
        if( ! send_result && ! out->has_error() )
            instance->log_error("send queue full");
        return send_result;
    }

    bool write_error_event(alpe::error_event e)
    {
        instance->log_error("sending error event (%d, %d)", int(e.type), e.alsa_errno);
        log_would_block(::write_error_event(out, e));
        return false;
    }

    bool write_configuration_event(alpe::configuration cfg)
    {
        frame_byte_size = lbu::alsa::pcm_format::frame_packed_byte_size(cfg.format.type, cfg.format.channels);
        return log_would_block(::write_configuration_event(out, cfg));
    }

    bool continue_read_command()
    {
        if( ! reader.read(in) )
            return ! (in->has_error() || in->at_end());

        const auto cmd = next_command;
        next_command = StreamCommand::Invalid;

        switch( cmd ) {
        case StreamCommand::Hello: {
            if( (std::memcmp(command_buffer, HelloMessage, 5) != 0)
                || ! is_digit(command_buffer[5]) || command_buffer[6] != '?' || command_buffer[7] != '\n' ) {
                command_buffer[8] = 0;
                instance->log_error("bad hello message '%s'", command_buffer);
                return false;
            }
            const size_t tmp = lbu::value_reinterpret_cast<char, uint8_t>(command_buffer[8]);
            if( tmp > 0 ) {
                next_command = StreamCommand::HelloExtended;
                reader.reset(lbu::array_ref<char>(command_buffer, tmp));
            }
            return log_would_block(write_hello_answer(out));
        }
        case StreamCommand::HelloExtended:
            return true;
        case StreamCommand::Configure: {
            const lbu::alsa::pcm_format requested_format = { snd_pcm_format_t(lbu::byte_reinterpret_cast<int32_t>(command_buffer)),
                                                             lbu::byte_reinterpret_cast<uint16_t>(command_buffer + 4),
                                                             lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 6) };
            const uint64_t ms = lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 10);

            auto cfg = instance->configure(requested_format, std::chrono::milliseconds(ms));
            if( std::holds_alternative<alpe::error_event>(cfg) )
                return write_error_event(std::get<alpe::error_event>(cfg));
            if( preset ) {
                cfg = instance->update_parameters(preset->generate(std::get<alpe::configuration>(cfg)));
                if( std::holds_alternative<alpe::error_event>(cfg) )
                    return write_error_event(std::get<alpe::error_event>(cfg));
            }
            return write_configuration_event(std::get<alpe::configuration>(cfg));
        }
        case StreamCommand::UpdateParameters: {
            const uint8_t additional_buffer_count = lbu::byte_reinterpret_cast<uint8_t>(command_buffer);
            alpe::short_duration_frames pre_underrun_trigger_frames = lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 1);
            alpe::short_duration_frames rewind_safeguard = lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 5);
            alpe::short_duration_frames emergency_pause_fade = lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 9);

            const auto cfg = instance->update_parameters({additional_buffer_count, pre_underrun_trigger_frames, rewind_safeguard,
                                                          emergency_pause_fade});
            if( std::holds_alternative<alpe::error_event>(cfg) )
                return write_error_event(std::get<alpe::error_event>(cfg));
            return write_configuration_event(std::get<alpe::configuration>(cfg));
        }
        case StreamCommand::FadeIn:
        case StreamCommand::FadeInPlaying:
        case StreamCommand::FadeOut: {
            const alpe::fade_parameter param = { alpe::fade_parameter::Type(lbu::byte_reinterpret_cast<int32_t>(command_buffer)),
                                                 lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 4) };

            if( cmd == StreamCommand::FadeIn || (cmd == StreamCommand::FadeInPlaying && instance->state() == alpe::State::Playing) ) {
                if( auto e = instance->fade_in(param); e )
                    return write_error_event(*e);
            } else if( cmd == StreamCommand::FadeOut && instance->state() != alpe::State::Ready ) {
                if( auto e = instance->fade_out(param); e )
                    return write_error_event(*e);
            }
            return true;
        }
        case StreamCommand::AddBuffer:
            if( add_buffer_frames == 0 ) {
                add_buffer_eos = lbu::byte_reinterpret_cast<bool>(command_buffer);
                add_buffer_frames = lbu::byte_reinterpret_cast<uint32_t>(command_buffer + 1);

                if( add_buffer_frames == 0 ) {
                    instance->log_error("invalid zero add buffer frame count");
                    return false;
                }
                const auto buf = instance->get_buffer().array_static_cast<char>();
                if( buf.byte_size() == 0 || ((buf.byte_size() % frame_byte_size) != 0) ) {
                    instance->log_error("unexpected instance buffer size %u", unsigned(buf.byte_size()));
                    return false;
                }

                reader.reset(buf.sub_first(add_buffer_frames * size_t(frame_byte_size)));
                next_command = StreamCommand::AddBuffer;
            } else {
                const auto frames = add_buffer_frames;
                auto buf = instance->get_buffer().array_static_cast<char>();
                const auto buf_frames = alpe::short_duration_frames(buf.byte_size() / frame_byte_size);
                const bool eos = (add_buffer_eos && buf_frames >= frames);

                add_buffer_frames = 0;

                if( auto e = instance->advance_buffer(std::min(frames, buf_frames),
                                                      eos ? alpe::EndOfStream::Yes : alpe::EndOfStream::No); e ) {
                    return write_error_event(*e);
                }

                if( buf_frames < frames ) {
                    add_buffer_frames = frames - buf_frames;
                    const size_t buffer_frames_bytes = add_buffer_frames * size_t(frame_byte_size);

                    buf = instance->get_buffer().array_static_cast<char>();
                    if( buf.byte_size() < buffer_frames_bytes || ((buf.byte_size() % frame_byte_size) != 0) ) {
                        instance->log_error("unexpected instance buffer size %u", unsigned(buf.byte_size()));
                        return false;
                    }
                    reader.reset(buf.sub_first(buffer_frames_bytes));
                    next_command = StreamCommand::AddBuffer;
                }
            }
            return true;
        default:
            instance->log_error("bad continue_read_command code %u", unsigned(cmd));
            return false;
        }
    }

    bool read_command()
    {
        if( next_command != StreamCommand::Invalid )
            return continue_read_command();

        uint8_t cmd_byte;
        if( auto r = in->read(&cmd_byte, 1, lbu::stream::Mode::NonBlocking); r <= 0 ) {
            if( in->has_error() || in->at_end() )
                return false;
            instance->log_warning("read_command called while no command ready to be read");
            return true;
        }
        if( cmd_byte > int(StreamCommand::Last) ) {
            instance->log_error("bad command code %u", cmd_byte);
            return false;
        }

        next_command = static_cast<StreamCommand>(cmd_byte);
        switch( next_command ) {
        case StreamCommand::Quit:
            instance->log_info("quit command received");
            return false;
        case StreamCommand::Configure:
            reader.reset(lbu::array_ref<char>(command_buffer, PcmFormatSize + 8));
            return true;
        case StreamCommand::UpdateParameters:
            reader.reset(lbu::array_ref<char>(command_buffer, 1 + 4 + 4 + 4));
            return true;
        case StreamCommand::FadeIn:
        case StreamCommand::FadeInPlaying:
        case StreamCommand::FadeOut:
            reader.reset(lbu::array_ref<char>(command_buffer, FadeParameterSize));
            return true;
        case StreamCommand::AddBuffer:
            reader.reset(lbu::array_ref<char>(command_buffer, 1 + 4));
            return true;
        case StreamCommand::DropBuffers:
            next_command = StreamCommand::Invalid;
            if( instance->state() == alpe::State::Playing ) {
                if( ! log_would_block(write_playback_stop_event(out, {1})) )
                    return false;
            }
            if( auto e = instance->drop_buffers(); e )
                return write_error_event(*e);
            return true;
        default:
            instance->log_error("unexpected command code %u", cmd_byte);
            return false;
        }
    }

    bool operator()(alpe::no_event) { return true; }
    bool operator()(alpe::playback_event e) { return log_would_block(write_playback_event(out, e)); }
    bool operator()(alpe::playback_stop_event e) { return log_would_block(write_playback_stop_event(out, e)); }
    bool operator()(alpe::playback_underrun_event e) { return log_would_block(write_playback_underrun_event(out, e)); }
    bool operator()(alpe::error_event e) {
        if( next_command == StreamCommand::Hello ) {
            instance->log_error("instance error event before hand shake finished");
            return false;
        }
        return write_error_event(e);
    }

    alpe_proxy(lbu::stream::abstract_input_stream* in_stream, lbu::stream::abstract_output_stream* out_stream,
               alpe* alpe_instance, alpe::engine_parameter_system_preset* system_preset)
        : in(in_stream), out(out_stream), instance(alpe_instance), preset(system_preset)
    {
        reader.reset(lbu::array_ref<char>(command_buffer, HelloSize));
    }
};

void alpe::proxy_run(lbu::stream::fd_input_stream* in_stream, lbu::stream::abstract_output_stream* out_stream,
                     alpe* instance, pollfd interrupt_fd, engine_parameter_system_preset* preset)
{
    std::vector<pollfd> pfds = {interrupt_fd, lbu::poll::poll_fd(in_stream->descriptor(), lbu::poll::FlagsReadReady)};
    constexpr int InterruptIndex = 0;
    constexpr int CommandStreamIndex = 1;
    {
        const auto inst_pfds = instance->get_poll_fds();
        pfds.insert(pfds.end(), inst_pfds.begin(), inst_pfds.end());
    }
    lbu::array_ref<pollfd> instance_pfds(&pfds[2], pfds.size() - 2);

    alpe_proxy proxy(in_stream, out_stream, instance, preset);

    while( true ) {
        const bool instance_has_buffered_events = instance->has_buffered_events();

        if( pfds[CommandStreamIndex].revents == 0 && ! instance_has_buffered_events )
            lbu::poll::wait_for_events(pfds.data(), pfds.size());

        if( pfds[InterruptIndex].revents ) {
            instance->log_info("proxy_run interrupted");
            break;
        }

        if( instance_has_buffered_events || alpe_utils::has_revent(instance_pfds) ) {
            if( ! std::visit(proxy, instance->next_event()) )
                break;
        }

        if( pfds[CommandStreamIndex].revents ) {
            if( ! proxy.read_command() )
                break;
            pfds[CommandStreamIndex].revents = (in_stream->buffer_read_available() == 0) ? 0 : lbu::poll::FlagsReadReady;
        }
    }

    if( instance->state() == alpe::State::Playing ) {
        instance->log_warning("quitting proxy_run while still playing - fading out...");
        instance->fade_out({fade_parameter::Type::Linear, instance->current_config.parameter.emergency_pause_fade});

        while( instance->state() == alpe::State::Playing ) {
            if( ! instance->has_buffered_events() )
                lbu::poll::wait_for_events(instance_pfds.data(), instance_pfds.size());
            instance->next_event();
        }
    }
}
