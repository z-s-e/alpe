/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "alpe_thread.h"

#include "alpe_utils.h"

#include <atomic>
#include <cfenv>
#include <lbu/eventfd.h>
#include <lbu/triple_buffer_spsc.h>
#include <pthread.h>
#include <vector>


//#define ALPE_THREAD_DEBUG_LOG_PLAYBACK_EVENT 1

namespace {

enum Command {
    CommandQuit = 1,
    CommandReset,
    CommandPlay,
    CommandPause,
    CommandSeek,
    CommandStop
};

constexpr auto NoSeek = std::numeric_limits<alpe::long_duration_frames>::max();

using steady_clock = std::chrono::steady_clock;
using Log = alpe::LogLevel;
using State = alpe_thread::State;

static lbu::unique_fd open_event_fd() { return lbu::event_fd::create(0, lbu::event_fd::FlagsNonBlock); }
static eventfd_t read_event_fd(lbu::fd fd) { return lbu::event_fd::read_value(fd); }
static void write_event_fd(lbu::fd fd, eventfd_t value = 1) { lbu::event_fd::write_value(fd, value); }


struct alpe_thread_shared_data {
    alpe_thread::interface* const interface = {};
    alpe::debug_logger* const logger = {};
    const lbu::unique_fd event_fd;
    const lbu::unique_fd command_fd;

    std::atomic<State> state = State::Initial;
    std::atomic_bool command_pending = false;
    std::atomic_bool position_update_wakeup = false;
    lbu::triple_buffer_spsc<alpe::playback_event> time_info;

    alpe::long_duration_frames command_seek = NoSeek; // these are written from the main thread while command_pending is false and read
    alpe::fade_parameter command_fade_out;            // from the alpe thread while command_pending is true, so a mutex is not needed
    alpe::fade_parameter command_fade_in;
    bool command_autoplay = true;

    alpe_thread_shared_data(alpe_thread::interface* i, alpe::debug_logger* l, lbu::unique_fd ev, lbu::unique_fd cmd)
        : interface(i), logger(l), event_fd(std::move(ev)), command_fd(std::move(cmd))
    {}
};


struct alpe_thread_private_main_loop {
    alpe_thread_shared_data* const shared = {};
    alpe_thread::interface* const interface = {};
    const lbu::fd event_fd;
    const lbu::fd cmd_fd;

    std::vector<pollfd> pfds; // first element is cmd_fd, then the alpe pfds, last the buffer request
    bool requesting_buffer = false;

    alpe* instance = {};
    alpe::fade_parameter post_underrun_fade_in;

    size_t period_byte_size = 0;
    alpe::short_duration_frames pre_start_buffer = 0;

    alpe::long_duration_frames current_offset = 0;

    alpe::long_duration_frames pending_seek = NoSeek;
    alpe::fade_parameter pending_fade_in;
    bool pending_reset_autoplay = true;


    bool is_playing() const
    {
        if( instance->state() == alpe::State::Playing ) {
            assert(int(shared->state.load()) > int(State::Paused));
            return true;
        } else {
            assert(instance->state() == alpe::State::Ready && shared->state == State::Paused);
            return false;
        }
    }

    void update_time_info(alpe::long_duration_frames position, steady_clock::time_point time)
    {
        shared->time_info.current_producer_buffer() = {position, time};
        shared->time_info.swap_producer_buffer();
    }

    void set_state_notify(State s)
    {
        shared->state = s;
        write_event_fd(event_fd);
    }

    bool clear_pending_notify()
    {
        shared->command_pending = false;
        write_event_fd(event_fd);
        return true;
    }

    bool set_state_clear_pending_notify(State s)
    {
        shared->state = s;
        return clear_pending_notify();
    }

    void reset_to_initial()
    {
        pfds.resize(1);
        instance = {};
        current_offset = 0;
        pending_seek = NoSeek;
        set_state_notify(State::Initial);
    }

    alpe_thread::interface::buffer_ready_info wait_for_buffer_request()
    {
        lbu::poll::wait_for_event(pfds.back());
        return interface->buffer_ready();
    }

    void blocking_discard_last_buffer_request()
    {
        if( ! requesting_buffer )
            return;
        wait_for_buffer_request();
        requesting_buffer = false;
    }

    void handle_error(alpe::error_event e)
    {
        assert(shared->state != State::Initial);
        interface->report_error(e);
        blocking_discard_last_buffer_request();
        reset_to_initial();
    }

    bool handle_command_error(alpe::error_event e)
    {
        shared->command_pending = false;
        handle_error(e);
        return true;
    }

    bool blocking_handle_last_buffer_request(alpe::short_duration_frames* frames_received = nullptr)
    {
        assert(requesting_buffer);
        auto buf_info = wait_for_buffer_request();
        if( buf_info.error ) {
            requesting_buffer = false;
            reset_to_initial();
            return false;
        }
        if( auto e = instance->advance_buffer(buf_info.frames, buf_info.eos); e ) {
            requesting_buffer = false;
            handle_error(*e);
            return false;
        }
        if( frames_received )
            *frames_received = buf_info.frames;
        return true;
    }

    lbu::array_ref<void> get_limited_instance_buffer(size_t limit)
    {
        return instance->get_buffer().array_static_cast<char>().sub_first(limit);
    }

    void start_playback()
    {
        set_state_notify(State::Buffering);

        if( requesting_buffer ) {
            if( ! blocking_handle_last_buffer_request() )
                return;
        }

        const auto current_buffered = instance->current_buffered_frames();
        if( current_buffered < pre_start_buffer && instance->get_buffer() ) {
            const auto fmt = instance->current_configuration().format;
            const size_t frame_byte_size = lbu::alsa::pcm_format::frame_packed_byte_size(fmt.type, fmt.channels);

            auto write_bytes = (pre_start_buffer - current_buffered) * frame_byte_size;
            requesting_buffer = true;

            auto buf = get_limited_instance_buffer(write_bytes);
            do {
                assert(buf);
                interface->request_buffer(buf);
                alpe::short_duration_frames frames_received = 0;
                if( ! blocking_handle_last_buffer_request(&frames_received) )
                    return;
                write_bytes -= (frames_received * frame_byte_size);
                buf = get_limited_instance_buffer(write_bytes);
            } while( write_bytes > 0 && buf );
        }
        requesting_buffer = false;

        if( instance->at_stream_end() ) {
            set_state_notify(State::Paused);
        } else {
            // waiting for the first playback event after fade_in would be more correct/precise,
            // but this is hopefully good enough:
            update_time_info(current_offset + instance->last_position(), steady_clock::now());
            if( auto e = instance->fade_in(pending_fade_in); e )
                handle_error(*e);
            else
                set_state_notify(State::Playing);
        }
    }

    void finalize_reset()
    {
        assert(shared->state == State::Resetting);

        blocking_discard_last_buffer_request();
        current_offset = 0;
        update_time_info(current_offset, steady_clock::now());

        auto r = interface->reset();
        instance = r.instance;
        post_underrun_fade_in = r.post_underrun_fade_in;

        pfds.resize(1);
        if( instance == nullptr ) {
            set_state_notify(State::Initial);
            return;
        }

        {
            auto inst_pfds = instance->get_poll_fds();
            pfds.insert(pfds.end(), inst_pfds.begin(), inst_pfds.end());
            pfds.push_back(r.buffer_ready_pollfd);
        }

        {
            const auto cfg = instance->current_configuration();
            const size_t frame_byte_size = lbu::alsa::pcm_format::frame_packed_byte_size(cfg.format.type, cfg.format.channels);
            period_byte_size = cfg.period_frames * frame_byte_size;
            pre_start_buffer = (cfg.buffer_count() - 1) * cfg.period_frames;
        }

        if( pending_reset_autoplay ) {
            pending_fade_in = {};
            start_playback();
        } else {
            set_state_notify(State::Paused);
        }
    }

    bool finalize_seek()
    {
        assert(pending_seek != NoSeek);
        assert(shared->state == State::Seeking);

        blocking_discard_last_buffer_request();
        if( auto e = instance->drop_buffers(); e ) {
            handle_error(*e);
            return false;
        }
        current_offset = interface->seek(pending_seek);
        pending_seek = NoSeek;
        update_time_info(current_offset, steady_clock::now());
        return true;
    }

    bool finalize_seek_command_while_paused()
    {
        assert(shared->state == State::Paused);
        set_state_clear_pending_notify(State::Seeking);
        return finalize_seek();
    }

    bool handle_command()
    {
        assert(shared->command_pending);
        const auto cmd = read_event_fd(cmd_fd);
        const auto current_state = shared->state.load();

        if( current_state == State::Resetting && cmd != CommandQuit && cmd != CommandReset ) {
            alpe::log(shared->logger, Log::Error, "API error - command %li received while resetting", long(cmd));
            return clear_pending_notify();
        }

        switch( cmd ) {
        case CommandQuit:
            blocking_discard_last_buffer_request();
            return false;
        case CommandReset:
            pending_seek = NoSeek;
            pending_reset_autoplay = shared->command_autoplay;
            if( is_playing() ) {
                if( auto e = instance->fade_out(shared->command_fade_out); e )
                    return handle_command_error(*e);
                return set_state_clear_pending_notify(State::Resetting);
            } else {
                shared->state = State::Resetting;
                shared->command_pending = false;
                finalize_reset();
                return true;
            }
        case CommandPlay:
            pending_seek = NoSeek;
            pending_fade_in = shared->command_fade_in;
            if( is_playing() ) {
                if( auto e = instance->fade_in(pending_fade_in); e )
                    return handle_command_error(*e);
                return set_state_clear_pending_notify(State::Playing);
            } else {
                if( instance->at_stream_end() ) {
                    pending_seek = 0;
                    if( ! finalize_seek_command_while_paused() )
                        return true;
                } else {
                    shared->command_pending = false;
                }
                start_playback();
                return true;
            }
        case CommandPause:
            pending_seek = NoSeek;
            if( is_playing() ) {
                if( auto e = instance->fade_out(shared->command_fade_out); e )
                    return handle_command_error(*e);
                return set_state_clear_pending_notify(State::FadingOut);
            } else {
                return clear_pending_notify();
            }
        case CommandSeek:
            pending_seek = shared->command_seek;
            pending_fade_in = shared->command_fade_in;
            if( is_playing() ) {
                if( auto e = instance->fade_out(shared->command_fade_out); e )
                    return handle_command_error(*e);
                if( current_state == State::Playing || shared->command_autoplay )
                    shared->state = State::Seeking;
                return clear_pending_notify();
            } else {
                const bool autoplay = shared->command_autoplay;
                if( ! finalize_seek_command_while_paused() )
                    return true;
                if( autoplay )
                    start_playback();
                else
                    set_state_notify(State::Paused);
                return true;
            }
        case CommandStop:
            pending_seek = 0;
            if( is_playing() ) {
                if( auto e = instance->fade_out(shared->command_fade_out); e )
                    return handle_command_error(*e);
                return set_state_clear_pending_notify(State::FadingOut);
            } else {
                if( ! finalize_seek_command_while_paused() )
                    return true;
                set_state_notify(State::Paused);
                return true;
            }
        default:
            assert(false);
            return false;
        }
    }

    bool wait_for_reset()
    {
        while( true ) {
            assert(shared->state == State::Initial && instance == nullptr && ! requesting_buffer);
            lbu::poll::wait_for_event(pfds.front());

            switch( read_event_fd(cmd_fd) ) {
            case CommandQuit:
                return false;
            case CommandPlay:
                shared->command_autoplay = true;
                [[fallthrough]];
            case CommandReset:
                pending_reset_autoplay = shared->command_autoplay;
                set_state_clear_pending_notify(State::Resetting);
                finalize_reset();
                if( shared->state != State::Initial )
                    return true;
                break;
            case CommandPause:
            case CommandSeek:
            case CommandStop:
                shared->command_pending = false;
                break;
            default:
                assert(false);
            }

            write_event_fd(event_fd);
        }
    }

    bool run_initialized()
    {
        constexpr int command_fd_index = 0;
        while( true ) {
            assert(shared->state != State::Initial && instance != nullptr && pfds.size() >= 3);
            const auto pfd_count = pfds.size();
            const auto buffer_request_fd_index = pfd_count - 1;
            const lbu::array_ref<const pollfd> instance_pfds(&pfds[1], pfd_count - 2);
            pfds[command_fd_index].revents = 0;
            pfds[buffer_request_fd_index].revents = 0;

            const bool instance_has_buffered_events = instance->has_buffered_events();

            if( ! instance_has_buffered_events )
                lbu::poll::wait_for_events(pfds.data(), pfd_count - (requesting_buffer ? 0 : 1));

            if( instance_has_buffered_events || alpe_utils::has_revent(instance_pfds) ) {
                std::visit(*this, instance->next_event());
                if( shared->state == State::Initial )
                    return true;
            }

            // Note: The instance event handling might have already handled (resp. discarded) the buffer request, so we
            //       need to check if requesting_buffer is still on here. It is not allowed for the instance handling
            //       to start a new asynchronous buffer request, as the revents value would be then outdated here.
            if( requesting_buffer && pfds[buffer_request_fd_index].revents ) {
                auto buf_info = interface->buffer_ready();
                requesting_buffer = false;
                if( buf_info.error ) {
                    reset_to_initial(); // TODO try emergency fade out here as well?
                    return true;
                }
                if( auto e = instance->advance_buffer(buf_info.frames, buf_info.eos); e ) {
                    handle_error(*e);
                    return true;
                }
            }
            if( ! requesting_buffer && instance->get_buffer() ) {
                interface->request_buffer(get_limited_instance_buffer(period_byte_size));
                requesting_buffer = true;
            }

            if( pfds[command_fd_index].revents ) {
                if( ! handle_command() )
                    return false;
                if( shared->state == State::Initial )
                    return true;
            }
        }
    }

    void run()
    {
        while( true ) {
            if( ! wait_for_reset() )
                return;
            if( ! run_initialized() )
                return;
        }
    }


    void handle_stop(alpe::long_duration_frames position)
    {
        update_time_info(position, steady_clock::now());

        switch( shared->state ) {
        case State::Playing:
            if( instance->at_stream_end() )
                set_state_notify(State::Paused);
            else
                start_playback();
            break;
        case State::Seeking:
            if( finalize_seek() )
                start_playback();
            break;
        case State::FadingOut:
            if( pending_seek != NoSeek ) {
                set_state_notify(State::Seeking);
                if( ! finalize_seek() )
                    return;
            }
            set_state_notify(State::Paused);
            break;
        case State::Resetting:
            finalize_reset();
            break;
        default:
            assert(false);
        }
    }


    void operator()(alpe::no_event) {}
    void operator()(alpe::playback_event e)
    {
        update_time_info(current_offset + e.position, e.time);
        if( shared->position_update_wakeup )
            write_event_fd(event_fd);

#ifdef ALPE_THREAD_DEBUG_LOG_PLAYBACK_EVENT
        alpe::log(shared->logger, Log::Info, "playback event %li @ %li", long(e.position), long(e.time.time_since_epoch().count()));
#endif
    }
    void operator()(alpe::playback_stop_event e) { handle_stop(current_offset + e.position); }
    void operator()(alpe::playback_underrun_event e)
    {
        e.position += current_offset;
        interface->report_underrun(e);
        pending_fade_in = post_underrun_fade_in;
        handle_stop(e.position);
    }
    void operator()(alpe::error_event e) { handle_error(e); }


    alpe_thread_private_main_loop(alpe_thread_shared_data* s)
        : shared(s), interface(s->interface), event_fd(s->event_fd.get()), cmd_fd(s->command_fd.get())
    {
        if( int err = alpe_thread::current_thread_set_realtime_priority(); err != 0 )
            alpe::log(s->logger, Log::Warning, "could not set realtime priority for audio thread: %s", strerror(err));

        pfds = { lbu::poll::poll_fd(cmd_fd, lbu::poll::FlagsReadReady) };
    }
};

static void* alpe_thread_main(void* shared_data_ptr)
{
    pthread_setname_np(pthread_self(), "alpe_thread");
    std::fesetround(FE_TONEAREST);
    alpe_thread_private_main_loop loop(static_cast<alpe_thread_shared_data*>(shared_data_ptr));
    loop.run();
    return {};
}

} // namespace


struct alpe_thread::data {
    alpe_thread_shared_data shared;
    pthread_t thread = {};

    void write_command(Command cmd)
    {
        if( shared.command_pending ) {
            alpe::log(shared.logger, Log::Error, "API error - trying to write command while other command pending");
            return;
        }
        shared.command_pending = true;
        write_event_fd(shared.command_fd.get(), cmd);
    }

    ~data()
    {
        while( shared.command_pending ) {
            lbu::poll::wait_for_event(lbu::poll::poll_fd(shared.event_fd.get(), lbu::poll::FlagsReadReady));
            read_event_fd(shared.event_fd.get());
        }

        write_command(CommandQuit);

        if( int err = pthread_join(thread, nullptr); err != 0 )
            lbu::unexpected_system_error(err);
    }

    data(interface* application_interface, alpe::debug_logger* logger)
        : shared(application_interface, logger, open_event_fd(), open_event_fd())
    {
        if( int err = pthread_create(&thread, nullptr, alpe_thread_main, &shared); err != 0 )
            lbu::unexpected_system_error(err);
    }
};


alpe_thread::alpe_thread(interface* application_interface, alpe::debug_logger* logger) : d(new data(application_interface, logger)) {}
alpe_thread::~alpe_thread() { delete d; }

pollfd alpe_thread::event_fd() const { return lbu::poll::poll_fd(d->shared.event_fd.get(), lbu::poll::FlagsReadReady); }
void alpe_thread::set_playback_position_update_wakeup(bool wakeup) { d->shared.position_update_wakeup.store(wakeup); }

alpe_thread::State alpe_thread::state() const
{
    read_event_fd(d->shared.event_fd.get());
    return d->shared.command_pending ? State::CommandPending : d->shared.state.load();
}

alpe::playback_event alpe_thread::last_playback_position()
{
    const alpe::playback_event e1 = d->shared.time_info.current_consumer_buffer();
    d->shared.time_info.swap_consumer_buffer();
    const alpe::playback_event e2 = d->shared.time_info.current_consumer_buffer();
    return e1.time > e2.time ? e1 : e2;
}

void alpe_thread::reset(alpe::fade_parameter fade_out)
{
    d->shared.command_fade_out = fade_out;
    d->shared.command_autoplay = false;
    d->write_command(CommandReset);
}

void alpe_thread::reset_autoplay(alpe::fade_parameter fade_out)
{
    d->shared.command_fade_out = fade_out;
    d->shared.command_autoplay = true;
    d->write_command(CommandReset);
}

void alpe_thread::play(alpe::fade_parameter fade_in)
{
    d->shared.command_fade_in = fade_in;
    d->write_command(CommandPlay);
}

void alpe_thread::pause(alpe::fade_parameter fade_out)
{
    d->shared.command_fade_out = fade_out;
    d->write_command(CommandPause);
}

void alpe_thread::seek(alpe::long_duration_frames pos, alpe::fade_parameter fade_out, alpe::fade_parameter fade_in)
{
    d->shared.command_seek = pos;
    d->shared.command_fade_out = fade_out;
    d->shared.command_fade_in = fade_in;
    d->shared.command_autoplay = false;
    d->write_command(CommandSeek);
}

void alpe_thread::seek_autoplay(alpe::long_duration_frames pos, alpe::fade_parameter fade_out, alpe::fade_parameter fade_in)
{
    d->shared.command_seek = pos;
    d->shared.command_fade_out = fade_out;
    d->shared.command_fade_in = fade_in;
    d->shared.command_autoplay = true;
    d->write_command(CommandSeek);
}

void alpe_thread::stop(alpe::fade_parameter fade_out)
{
    d->shared.command_fade_out = fade_out;
    d->write_command(CommandStop);
}

void alpe_thread::wait_for_playback_finish()
{
    auto pfd = lbu::poll::poll_fd(d->shared.event_fd.get(), lbu::poll::FlagsReadReady);
    while( true ) {
        if( const auto s = state(); s == State::Paused || s == State::Initial )
            return;
        lbu::poll::wait_for_event(pfd);
    }
}

int alpe_thread::current_thread_set_realtime_priority()
{
    struct sched_param param = {};
    param.__sched_priority = sched_get_priority_max(SCHED_RR) - 1;
    return pthread_setschedparam(pthread_self(), SCHED_RR, &param);
}
