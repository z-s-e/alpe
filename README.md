# Advanced Linux Playback Engine

This is a small static C++ library that implements a playback engine on top of Alsa, i.e. the playback logic of an audio/music player application.

It tries extra hard to be as (audio-) glitch free as possible and has two main features that to my knowledge no other Linux audio player offers (or at least, not quite in the way I want):

- Well implemented playback fade-in or fade-out to avoid pops (common in other players when pausing/resuming)
- Has a simple pcm (thus lossless) streaming protocol and backend

For more information see also the [intro article](docs/intro.md). An example audio player using this engine can be found [here](https://gitlab.com/z-s-e/alpela) and an example playback streaming server [here](https://gitlab.com/z-s-e/mapla). The streaming protocol's high level [state and message exchange spec](docs/alpe_stream.nfqda) is formulated and verified using my [NFQDA formalism](https://gitlab.com/z-s-e/nfqda).

It uses my [utility library](https://github.com/z-s-e/lbu), which must be present for it to build.

Required (pkg-config) packages are `alsa`, and when compiling with the optional PipeWire support (by using `-DALPE_BUILD_PIPEWIRE=On`) also `libpipewire-0.3`.


## Known issues

The engine currently does not work correctly with the PulseAudio Alsa plugin - This seems to be an issue of PulseAudio not behaving like regular Alsa devices. With PipeWire's Alsa plugin it does work fairly well, but the dedicated PipeWire support still should likely be preferred.
